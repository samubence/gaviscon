#include "ofApp.h"
#include "xioAllElements.h"
#include "LetterBoxView.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetFrameRate(25);

    ofSetWindowPosition(ofGetScreenWidth() - ofGetWidth() - 8, 25);
    ofEnableAlphaBlending();
    ofSetLogLevel(OF_LOG_VERBOSE);

    compWidth = 600;
    compHeight = 400;
    numOfImages = 4;
    shootInterval = 1.5;
    rootDir = "rootDir";

    ofxXmlSettings xml;
    if (xml.loadFile("settings.xml"))
    {
        rootDir = xml.getValue("settings:rootDir", rootDir, 0);
        compWidth = xml.getValue("settings:compWidth", compWidth, 0);
        compHeight = xml.getValue("settings:compHeight", compHeight, 0);
    }

    bool canonInited = photoServer.start();
    if (!canonInited)
    {
        printf("ERROR: Camera not connected!\n");
    }
    ofAddListener(photoServer.imageDownloadedEvent, this, &ofApp::imageDownloadedEvent);

    xio.setStyle(new xioDefaultStyleAlpha());
    xioWindow * w = xio.createWindow("settings", 0, 0, 300, ofGetHeight());
    w->addGroup("Canon");
    w->addElement(this, new xio_SliderList("ISO", photoServer.getIsoSpeedValues(), 0), XIO_ISO);
    w->addElement(this, new xio_SliderList("Exposure", photoServer.getExposureValues(), 0), XIO_EXPOSURE);
    w->addElement(this, new xio_SliderList("Aperture", photoServer.getApertureValues(), 0), XIO_APERTURE);
    w->addGroup("photo");
    w->addElement(this, new xio_Slider("numOfImages", 1, 10, &numOfImages));
    w->addElement(this, new xio_Slider("shootInterval", 1, 60, &shootInterval));
    w->addElement(this, new xio_PushButton("test"), XIO_SINGLE_SHOOT);
    w->addElement(this, new xio_PushButton("save"), XIO_SAVE);
    xio.load("gui.xml");

    fileName = "default";
    savingPictures = false;
    comp.allocate(compWidth, compHeight, GL_RGB);

    oscReceiver.setup(54321);
}

//--------------------------------------------------------------
void ofApp::update()
{
    while (oscReceiver.hasWaitingMessages())
    {
        ofxOscMessage m;
        oscReceiver.getNextMessage(&m);

        if (m.getAddress() == "shoot")
        {
            fileName = m.getArgAsString(0);
            start();
        }
    }
    photoServer.update();

    if (frames.size() >= numOfImages && savingPictures)
    {
        savingPictures = false;

        comp.begin();
        ofClear(0);

        int x = 0;
        int y = 0;
        for (int i = 0; i < frames.size(); i++)
        {
            ofSetColor(255);
            frames[i]->draw(x * 300, y * 200, 300, 200);
            x ++;
            if (x >= 2)
            {
                x = 0;
                y ++;
            }
        }
        comp.end();

        ofPixels pixels;
        comp.readToPixels(pixels);
        ofSaveImage(pixels, fileName + ".jpg");
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(0);
    if (frames.size() >= numOfImages)
    {
        LetterBoxView::begin(ofRectangle(0, 0, comp.getWidth(), comp.getHeight()), ofRectangle(300, 0, ofGetWidth() - 300, ofGetHeight()));
        ofSetColor(255);
        comp.draw(0, 0);
        LetterBoxView::end(ofRectangle(0, 0, comp.getWidth(), comp.getHeight()));
    }
    if (photoServer.testImage.isAllocated())
    {
        ofSetColor(255);
        LetterBoxView::begin(ofRectangle(0, 0, photoServer.testImage.getWidth(), photoServer.testImage.getHeight()), ofRectangle(300, 0, ofGetWidth() - 300, ofGetHeight()));
        photoServer.testImage.draw(0, 0);
        LetterBoxView::end(ofRectangle(0, 0, photoServer.testImage.getWidth(), photoServer.testImage.getHeight()));
    }

    xio.draw();
}

void ofApp::start()
{
    photoServer.testImage.clear();

    for (int i = 0; i < frames.size(); i++) delete frames[i];
    frames.clear();

    savingPictures = true;

    if (photoServer.cameraInited)
    {
        photoServer.shootPicture(numOfImages, shootInterval, fileName);
    }
}

void ofApp::imageDownloadedEvent(PhotoServerEventArgs & e)
{
    //ofSaveImage(e.pixels, "raw/" + e.fileName);
    ofImage * img = new ofImage();
    img->setFromPixels(e.pixels);
    frames.push_back(img);
}

void ofApp::xioEvent(xioEventArgs & e)
{
    if (e.id == XIO_ISO)
    {
        photoServer.setIsoSpeedValue(e.getInt());
    }
    if (e.id == XIO_EXPOSURE)
    {
        photoServer.setExposureValue(e.getInt());
    }
    if (e.id == XIO_APERTURE)
    {
        photoServer.setApertureValue(e.getInt());
    }
    if (e.id == XIO_SINGLE_SHOOT)
    {
        for (int i = 0; i < frames.size(); i++)
        {
            delete frames[i];
        }
        frames.clear();

        photoServer.singleShoot();
    }
    if (e.id == XIO_SAVE)
    {
        xio.save("gui.xml");
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == ' ')
    {
        start();
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::exit()
{
    photoServer.stop();
}
