#pragma once

#include "ofMain.h"
#include "PhotoServer.h"
#include "XIO.h"
#include "ofxOsc.h"

enum
{
    XIO_ISO,
    XIO_EXPOSURE,
    XIO_APERTURE,
    XIO_SINGLE_SHOOT,
    XIO_SAVE
};

class ofApp : public ofBaseApp, xioListener{

	public:
		void setup();
		void exit();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void xioEvent(xioEventArgs & e);
		void imageDownloadedEvent(PhotoServerEventArgs & e);

		void start();

		PhotoServer photoServer;
		XIO xio;
		ofxOscReceiver oscReceiver;

		int compWidth, compHeight;
		int numOfImages;
		bool startPhoto;
		string rootDir;
		float shootInterval;
		string fileName;
		bool savingPictures;
		vector<ofImage*> frames;

		ofFbo comp;

};
