#ifndef __xio_EdsPropSlider_h__
#define __xio_EdsPropSlider_h__

#include "ofMain.h"
#include "xioBaseElement.h"
#include "EdsWrapper.h"

class xio_EdsPropSlider : public xioBaseElement
{
    public:
        xio_EdsPropSlider(string _name, map<EdsUInt32, const char *> _propTable, int defaultValue)
        {
            xioData.name = _name;
            propTable = _propTable;
            valueMin = 0;
            valueMax = propTable.size() - 1;
            valuePercent = ofMap(defaultValue, valueMin, valueMax, 0, 1);
            xioData.value = defaultValue;
            /*
            printf("%s\n", _name.c_str());
            for (int i = 0; i < textArray.size(); i++) printf("%s\n", textArray[i].c_str());
            */
        };


        ~xio_EdsPropSlider(){};

        void update()
        {

        };

        void draw()
        {

            float x = valuePercent * getWidth();

            parent->useElementFillColor();
            ofFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementFontColor();
            //if (textArray.size() > 0) parent->drawString(xioData.name + " : " + textArray[(int)xioData.value], 0, 0, getWidth(), getHeight(), XIO_ALIGN_CENTER);
        };

        void mousePressed(float x, float y, int button)
        {
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            xioData.value = (int)xioData.value;

            notifyEvent();
            updateValues();
        };

        void updateValues()
        {
            valuePercent = ofMap(xioData.value, valueMin, valueMax, 0, 1);
        };

        void mouseDragged(float x, float y, int button)
        {
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            xioData.value = (int)xioData.value;
            notifyEvent();
            updateValues();
        };

        float valuePercent;
        float valueMin, valueMax;
};

#endif

