#ifndef __EdsPropStrings_h__
#define __EdsPropStrings_h__

#include "ofMain.h"

class EdsBasePropertyTable
{
public:
    map<EdsUInt32, const char *> _propertyTable;
};
class EdsAvTable : public EdsBasePropertyTable
{
public:
    EdsAvTable()
    {
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x00,"00"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x08,"1"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x0B,"1.1"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x0C,"1.2"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x0D,"1.2"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x10,"1.4"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x13,"1.6"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x14,"1.8"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x15,"1.8"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x18,"2"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1B,"2.2"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1C,"2.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1D,"2.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x20,"2.8"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x23,"3.2"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x24,"3.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x25,"3.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x28,"4"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2B,"4.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2C,"4.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2D,"5.0"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x30,"5.6"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x33,"6.3"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x34,"6.7"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x35,"7.1"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x38,"8"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3B,"9"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3C,"9.5"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3D,"10"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x40,"11"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x43,"13"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x44,"13"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x45,"14"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x48,"16"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4B,"18"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4C,"19"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4D,"20"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x50,"22"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x53,"25"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x54,"27"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x55,"29"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x58,"32"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5B,"36"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5C,"38"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5D,"40"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x60,"45"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x63,"51"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x64,"54"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x65,"57"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x68,"64"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6B,"72"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6C,"76"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6D,"80"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x70,"91"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0xffffffff,"unknown"));
    };
};
class EdsTvTable : public EdsBasePropertyTable
{
    public:
        EdsTvTable()
        {
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x0c,"Bulb"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x10,"30h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x13,"25h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x14,"20h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x15,"20h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x18,"15h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1B,"13h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1C,"10h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x1D,"10h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x20,"8h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x23,"6h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x24,"6h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x25,"5h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x28,"4h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2B,"3h2"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2C,"3h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x2D,"2h5"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x30,"2h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x33,"1h6"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x34,"1h5"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x35,"1h3"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x38,"1h"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3B,"0h8"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3C,"0h7"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x3D,"0h6"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x40,"0h5"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x43,"0h4"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x44,"0h3"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x45,"0h3"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x48,"4"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4B,"5"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4C,"6"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4D,"6"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x50,"8"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x53,"10"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x54,"10"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x55,"13"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x58,"15"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5B,"20"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5C,"20"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5D,"25"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x60,"30"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x63,"40"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x64,"45"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x65,"50"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x68,"60"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6B,"80"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6C,"90"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6D,"100"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x70,"125"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x73,"160"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x74,"180"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x75,"200"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x78,"250"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x7B,"320"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x7C,"350"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x7D,"400"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x80,"500"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x83,"640"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x84,"750"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x85,"800"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x88,"1000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x8B,"1250"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x8C,"1500"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x8D,"1600"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x90,"2000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x93,"2500"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x94,"3000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x95,"3200"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x98,"4000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x9B,"5000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x9C,"6000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x9D,"6400"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0xA0,"8000"));
            _propertyTable.insert( std::pair<EdsUInt32, const char *>(0xffffffff,"unknown"));
        }
};
class EdsIsoSpeedTable : public EdsBasePropertyTable
{
public:
    EdsIsoSpeedTable()
    {
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x00,"Auto"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x28,"6"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x30,"12"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x38,"25"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x40,"50"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x48,"100"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4b,"125"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x4d,"160"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x50,"200"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x53,"250"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x55,"320"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x58,"400"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5b,"500"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x5d,"640"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x60,"800"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x63,"1000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x65,"1250"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x68,"1600"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6b,"2000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x6d,"2500"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x70,"3200"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x73,"4000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x75,"5000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x78,"6400"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x7b,"8000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x7d,"10000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x80,"12800"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x83,"16000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x85,"20000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x88,"25600"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x8b,"32000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x8d,"40000"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x90,"51200"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0x98,"102400"));
        _propertyTable.insert( std::pair<EdsUInt32, const char *>(0xffffffff,"unknown"));
      }
};

#endif

