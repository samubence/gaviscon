#include "PhotoServer.h"

EdsError EDSCALLBACK PhotoServer::handleObjectEvent(EdsObjectEvent event, EdsBaseRef object, EdsVoid* context)
{
    ofLogVerbose() << "object event " << Eds::getObjectEventString(event);
    if(object) {
        if(event == kEdsObjectEvent_DirItemCreated) {
            ((PhotoServer*) context)->setDownloadImage(object);
        } else if(event == kEdsObjectEvent_DirItemRemoved) {
            // no need to release a removed item
        } else {
            try {
                Eds::SafeRelease(object);
            } catch (Eds::Exception& e) {
                ofLogError() << "Error while releasing EdsBaseRef inside handleObjectEvent()";
            }
        }
    }
    return EDS_ERR_OK;
}

EdsError EDSCALLBACK PhotoServer::handlePropertyEvent(EdsPropertyEvent event, EdsPropertyID propertyId, EdsUInt32 param, EdsVoid* context)
{
    ofLogVerbose() << "property event " << Eds::getPropertyEventString(event) << ": " << Eds::getPropertyIDString(propertyId) << " / " << param;
    if(propertyId == kEdsPropID_Evf_OutputDevice) {
        ((PhotoServer*) context)->setLiveReady();
    }
    return EDS_ERR_OK;
}

EdsError EDSCALLBACK PhotoServer::handleCameraStateEvent(EdsStateEvent event, EdsUInt32 param, EdsVoid* context)
{
    ofLogVerbose() << "camera state event " << Eds::getStateEventString(event) << ": " << param;
    if(event == kEdsStateEvent_WillSoonShutDown) {
        ((PhotoServer*) context)->setSendKeepAlive();
    }
    if (event == kEdsStateEvent_Shutdown) {
        ((PhotoServer*) context)->cameraShutdown();
    }
    return EDS_ERR_OK;
}

PhotoServer::PhotoServer()
{
    numOfShoots = 1;
    currentShoot = 0;
    shootInterval = 0;
    pShootTime = 0;
    shooting = false;
    fileName = "";
    cameraInited = false;
    doSingleShoot = false;
}

PhotoServer::~PhotoServer()
{
    stop();
}

bool PhotoServer::start()
{
    int deviceId = 0;
    try
    {
        Eds::InitializeSDK();

        EdsCameraListRef cameraList;
        Eds::GetCameraList(&cameraList);

        UInt32 cameraCount;
        Eds::GetChildCount(cameraList, &cameraCount);

        if(cameraCount > 0) {
            EdsInt32 cameraIndex = deviceId;
            Eds::GetChildAtIndex(cameraList, cameraIndex, &camera);
            Eds::SetObjectEventHandler(camera, kEdsObjectEvent_All, handleObjectEvent, this);
            Eds::SetPropertyEventHandler(camera, kEdsPropertyEvent_All, handlePropertyEvent, this);
            Eds::SetCameraStateEventHandler(camera, kEdsStateEvent_All, handleCameraStateEvent, this);

            EdsDeviceInfo info;
            Eds::GetDeviceInfo(camera, &info);
            Eds::SafeRelease(cameraList);
            ofLogVerbose("ofxEdsdk::setup") << "connected camera model: " <<  info.szDeviceDescription << " " << info.szPortName << endl;

            Eds::OpenSession(camera);

            // Lock UI
            Eds::SendStatusCommand(camera, kEdsCameraStatusCommand_UILock, 0);

            cameraInited = true;
            return true;
        } else {
            ofLogError() << "No cameras are connected for ofxEds::PhotoServer::setup().";
            return false;
        }
    }
    catch (Eds::Exception e)
    {
        ofLogError() << "PhotoServer: ERROR: start: " << e.what();
        return false;
    }

}

void PhotoServer::stop()
{
    if (!cameraInited) return;

    Eds::CloseSession(camera);
    Eds::TerminateSDK();

    cameraInited = false;
}

void PhotoServer::singleShoot()
{
    try
    {
        Eds::SendCommand(camera, kEdsCameraCommand_TakePicture, 0);
        doSingleShoot = true;
    }
    catch (Eds::Exception e)
    {
        ofLogError() << "PhotoServer: ERROR shootPicture: " << e.what();
    }
}

void PhotoServer::update()
{
    if (!cameraInited) return;
    if (shooting)
    {
        if (ofGetElapsedTimef() > pShootTime + shootInterval)
        {
            try
            {
                if (currentShoot < numOfShoots)
                {
                    Eds::SendCommand(camera, kEdsCameraCommand_TakePicture, 0);
                    pShootTime = ofGetElapsedTimef();
                    currentShoot ++;
                }
            }
            catch (Eds::Exception e)
            {
                ofLogError() << "PhotoServer: ERROR shootPicture: " << e.what();
            }
        }
    }
    else
    {
        if (directoryItems.size() > 0)
        {
            ofLogVerbose() << "Downloading images: " << directoryItems.size();
            for (int i = 0; i < directoryItems.size(); i++)
            {
                try
                {
                    ofBuffer imageBuffer;
                    EdsStreamRef stream = NULL;
                    EdsDirectoryItemInfo dirItemInfo;
                    Eds::GetDirectoryItemInfo(directoryItems[i], &dirItemInfo);
                    Eds::CreateMemoryStream(0, &stream);
                    Eds::Download(directoryItems[i], dirItemInfo.size, stream);
                    Eds::DownloadComplete(directoryItems[i]);
                    Eds::CopyStream(stream, imageBuffer);
                    Eds::DeleteDirectoryItem(directoryItems[i]);
                    Eds::SafeRelease(stream);

                    string fn = fileName + "_" + ofToString(i) + ".jpg";
                    ofPixels photoPixels;
                    ofLoadImage(photoPixels, imageBuffer);
                    PhotoServerEventArgs e;
                    e.fileName = fn;
                    e.pixels = photoPixels;
                    ofNotifyEvent(imageDownloadedEvent, e);
                }
                catch (Eds::Exception e)
                {
                    ofLogError() << "PhotoServer: ERROR: downloadImage: " << e.what();
                }
            }
            directoryItems.clear();
            printf("DONE\n");
        }
    }
}

void PhotoServer::shootPicture(int _numOfShoots, float _shootInterval, string _fileName)
{
    if (cameraInited)
    {
        numOfShoots = _numOfShoots;
        shootInterval = _shootInterval;
        fileName = _fileName;
        shooting = true;
        currentShoot = 0;
        pShootTime = ofGetElapsedTimef() - shootInterval;
    }
}

void PhotoServer::setLiveReady() {
    printf("LiveViewReady\n");
}

void PhotoServer::setDownloadImage(EdsDirectoryItemRef _directoryItem)
{
    ofLogVerbose() << "Picture created Event";

    if (doSingleShoot)
    {
        try
        {
            ofBuffer imageBuffer;
            EdsStreamRef stream = NULL;
            EdsDirectoryItemInfo dirItemInfo;
            Eds::GetDirectoryItemInfo(_directoryItem, &dirItemInfo);
            Eds::CreateMemoryStream(0, &stream);
            Eds::Download(_directoryItem, dirItemInfo.size, stream);
            Eds::DownloadComplete(_directoryItem);
            Eds::CopyStream(stream, imageBuffer);
            Eds::DeleteDirectoryItem(_directoryItem);
            Eds::SafeRelease(stream);

            string fn = "test.jpg";

            ofLoadImage(testImage, imageBuffer);

        }
        catch (Eds::Exception e)
        {
            ofLogError() << "PhotoServer: ERROR: downloadImage: " << e.what();
        }

        doSingleShoot = false;
    }
    else
    {
        if (directoryItems.size() < numOfShoots)
        {
            directoryItems.push_back(_directoryItem);
        }

        if (directoryItems.size() >= numOfShoots) shooting = false;
    }
}

void PhotoServer::setSendKeepAlive() {
    printf("KeepAlive\n");
    try
    {
        Eds::SendStatusCommand(camera, kEdsCameraCommand_ExtendShutDownTimer, 0);
    }
    catch (Eds::Exception& e)
    {
        ofLogError() << "Error while sending kEdsCameraCommand_ExtendShutDownTimer with Eds::SendStatusCommand: " << e.what();
    }
}

void PhotoServer::cameraShutdown() {
    printf("cameraShutDown\n");
    Eds::CloseSession(camera);
    Eds::TerminateSDK();
    start();
}

vector<string> PhotoServer::getIsoSpeedValues()
{
    vector<string> values;
    if (cameraInited)
    {
        EdsUInt32 propID = kEdsPropID_ISOSpeed;
        EdsPropertyDesc desc;
        Eds::GetPropertyDesc(camera, propID, &desc);
        for (int i = 0; i < desc.numElements; i++)
        {
            values.push_back(isoSpeedTable._propertyTable[desc.propDesc[i]]);
        }
    }
    return values;
}

vector<string> PhotoServer::getExposureValues()
{
    vector<string> values;
    if (cameraInited)
    {
        EdsUInt32 propID = kEdsPropID_Tv;
        EdsPropertyDesc desc;
        Eds::GetPropertyDesc(camera, propID, &desc);
        for (int i = 0; i < desc.numElements; i++)
        {
            values.push_back(tvTable._propertyTable[desc.propDesc[i]]);
        }
    }
    return values;
}

vector<string> PhotoServer::getApertureValues()
{
    vector<string> values;
    if (cameraInited)
    {
        EdsUInt32 propID = kEdsPropID_Av;
        EdsPropertyDesc desc;
        Eds::GetPropertyDesc(camera, propID, &desc);
        for (int i = 0; i < desc.numElements; i++)
        {
            values.push_back(avTable._propertyTable[desc.propDesc[i]]);
        }
    }
    return values;
}

void PhotoServer::setIsoSpeedValue(int valueID)
{
    if (!cameraInited) return;
    EdsPropertyDesc desc;
    Eds::GetPropertyDesc(camera, kEdsPropID_ISOSpeed, &desc);
    EdsUInt32 value = desc.propDesc[(int)ofClamp(valueID, 0, desc.numElements - 1)];
    try
    {
        Eds::SetPropertyData(camera, kEdsPropID_ISOSpeed, 0, sizeof(value), &value);
    }
    catch (Eds::Exception e)
    {
        ofLogError() << "PhotoServer: ERROR: setIsoSpeedValue: " << e.what();
    }
}

void PhotoServer::setExposureValue(int valueID)
{
    if (!cameraInited) return;
    EdsPropertyDesc desc;
    Eds::GetPropertyDesc(camera, kEdsPropID_Tv, &desc);
    EdsUInt32 value = desc.propDesc[(int)ofClamp(valueID, 0, desc.numElements - 1)];
    try
    {
        Eds::SetPropertyData(camera, kEdsPropID_Tv, 0, sizeof(value), &value);
    }
    catch (Eds::Exception e)
    {
        ofLogError() << "PhotoServer: ERROR: setExposureValue: " << e.what();
    }
}

void PhotoServer::setApertureValue(int valueID)
{
    if (!cameraInited) return;
    EdsPropertyDesc desc;
    Eds::GetPropertyDesc(camera, kEdsPropID_Av, &desc);
    EdsUInt32 value = desc.propDesc[(int)ofClamp(valueID, 0, desc.numElements - 1)];
    try
    {
        Eds::SetPropertyData(camera, kEdsPropID_Av, 0, sizeof(value), &value);
    }
    catch (Eds::Exception e)
    {
        ofLogError() << "PhotoServer: ERROR: setApertureValue: " << e.what();
    }
}
