#ifndef __PhotoServer_h__
#define __PhotoServer_h__

#include "ofMain.h"
#include "EdsWrapper.h"
#include "EdsPropertyTables.h"

class PhotoServerEventArgs
{
public:
    string fileName;
    ofPixels pixels;
};

class PhotoServer
{
    public:
        PhotoServer();
        ~PhotoServer();

        bool start();
        void stop();

        void update();

        void singleShoot();

        static EdsError EDSCALLBACK handleObjectEvent(EdsObjectEvent event, EdsBaseRef object, EdsVoid* context);
		static EdsError EDSCALLBACK handlePropertyEvent(EdsPropertyEvent event, EdsPropertyID propertyId, EdsUInt32 param, EdsVoid* context);
		static EdsError EDSCALLBACK handleCameraStateEvent(EdsStateEvent event, EdsUInt32 param, EdsVoid* context);

        void setLiveReady();
		void setDownloadImage(EdsDirectoryItemRef directoryItem);
		void setSendKeepAlive();
		void cameraShutdown();

		void shootPicture(int _numOfShoots = 1, float _shootInterval = 0, string _fileName = "");
		vector<string> getIsoSpeedValues();
        vector<string> getExposureValues();
        vector<string> getApertureValues();
        void setIsoSpeedValue(int valueID);
        void setExposureValue(int valueID);
        void setApertureValue(int valueID);

        ofEvent<PhotoServerEventArgs> imageDownloadedEvent;

        EdsCameraRef camera;
        bool cameraInited;

        vector<EdsDirectoryItemRef> directoryItems;

        EdsIsoSpeedTable isoSpeedTable;
        EdsTvTable tvTable;
        EdsAvTable avTable;

        int numOfShoots;
        int currentShoot;
        float shootInterval, pShootTime;
        bool shooting;
        string fileName;
        bool doSingleShoot;
        ofTexture testImage;
};

#endif

