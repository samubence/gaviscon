#pragma once

#include "ofMain.h"
#include "RenderList.h"
#include "Object.h"
#include "FireParticle.h"
#include "WaterParticle.h"
#include "Gun.h"
#include "Fire.h"
#include "Layer.h"
#include "ofxOsc.h"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
    void addObject();
    void addWater();
    
    void updateObjects();
    void updateFire();
    void updateWater();
    
    ofCamera cam;
    ofTexture flameTexture, ball, sausage;
    RenderList renderList;
    Gun gun;
    
    vector<Object*> objs;
    vector<FireParticle*> firePts;
    vector<WaterParticle*> waterPts;
    vector<Fire*> fire;
    
    Layer lLevel, lMid, lBg;
    
    bool moving;
    ofVec3f camPos;
    
    ofxOscReceiver oscReceiver;
    
};
