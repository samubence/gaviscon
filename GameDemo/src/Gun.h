//
//  Gun.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Gun_h
#define GameplayDemo_Gun_h

#include "ofMain.h"
#include "RenderListElement.h"
#include "ofx3DModelLoader.h"

/*
 TASKS:
    aim to cursor position
    get direction to shoot water particles
*/

class Gun : public ofNode, public RenderListElement
{
public:
    
    void setup(ofVec3f _p)
    {
        p = _p;
        setGlobalPosition(p);
        model.loadModel("gun.3ds", 100);
        light.setPointLight();
        light.setGlobalPosition(ofVec3f(100, 100, 50) + p);
        ofSetSmoothLighting(true);
    }
    
    ofVec3f getDirection()
    {
        float n = 0.05;
        return getLookAtDir() + ofVec3f(ofRandom(-n, n), ofRandom(-n, n), 0);
    }
    
    void draw()
    {
        transformGL();
        ofSetColor(255);
        //ofEnableLighting();
        //light.enable();
        /*
        ofFill();
        ofDrawBox(15, 15, 40);
        ofSetColor(200);
        ofDrawBox(0, 0, -20, 17, 17, 5);
         */
        ofRotate(90, 0, 0, 1);
        ofRotate(90, 0, 1, 0);

        model.draw();
        //light.disable();
        //ofDisableLighting();
        restoreTransformGL();
    }
    
    ofx3DModelLoader model;
    ofLight light;
    
};


#endif
