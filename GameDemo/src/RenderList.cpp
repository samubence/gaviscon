#include "RenderList.h"

RenderList::RenderList()
{

}

RenderList::~RenderList()
{

}

void RenderList::draw()
{
    // do the z sort
    std::sort(renderList.begin(), renderList.end(), compareZ);
    // draw
    for (vector<RenderListElement*>::iterator i = renderList.begin(); i != renderList.end(); i++)
    {
        (*i)->draw();
    }
}

void RenderList::add(RenderListElement * element)
{
    renderList.push_back(element);
}


void RenderList::clear()
{
    renderList.clear();
}