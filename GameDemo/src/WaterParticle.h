//
//  WaterParticle.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_WaterParticle_h
#define GameplayDemo_WaterParticle_h

#include "ofMain.h"
#include "RenderListElement.h"

/*
 TASKS:
    3D particle
    physics
    collision when hitting screen plane
*/
class WaterParticle : public RenderListElement
{
public:
    WaterParticle(ofVec3f _p, ofVec3f _v, ofTexture * _texture)
    {
        p = _p;
        v = _v;
        texture = _texture;
        alive = true;
        friction = 1;//0.99998;
        time = 0;
    }
    
    void update()
    {
        float g = -0.12;

        v += ofVec3f(0, g, 0);
        v *= friction;
        p += v;
        
        if (p.y < -1000)// || p.z < 0)
        {
            alive = false;
        }
        time++;
    }
    
    void draw()
    {
        float r = 20;
        
        ofPushMatrix();
        ofTranslate(p);
        float s = ofMap(time, 0, 10, 0, 1, true);
        ofScale(s, s);
        ofSetColor(255);
        ofFill();
        texture->draw(-r / 2., -r / 2., r, r);
        ofPopMatrix();
        
    }
    
    void collide()
    {
        alive = false;
    }
    
    bool alive;
    ofVec3f v;
    float friction;
    ofTexture * texture;
    int time;
};

#endif
