//
//  Layer.h
//  GameDemo
//
//  Created by bence samu on 23/07/14.
//
//

#ifndef GameDemo_Layer_h
#define GameDemo_Layer_h

#include "ofMain.h"
#include "RenderListElement.h"

class Layer : public RenderListElement
{
public:
    ofImage map, map2;
    unsigned char * pixels;
    ofShader shader;
    bool useShader;
    ofFbo maskFbo, fbo;
    float scale;
    ofVec2f collisionPoint;
    
    Layer()
    {
        
    };
    
    void setup(string fileName, string fileName2, ofVec3f _p, float _scale)
    {
        map.loadImage(fileName);
        map2.loadImage(fileName2);
        pixels = map.getPixels();
        p = _p;
        scale = _scale;
        useShader = true;
        setupShader();
    }
    
    void setup(string fileName, ofVec3f _p, float _scale)
    {
        map.loadImage(fileName);
        pixels = map.getPixels();
        p = _p;
        scale = _scale;
        useShader = false;
    }
    
    void draw()
    {
        ofSetColor(255);
        ofPushMatrix();
        ofTranslate(p);
        ofScale(scale, scale);
        if (useShader)
        {
            
            map.draw(0, 0);
            
            fbo.begin();
            ofClear(0, 0, 0, 0);
            
            shader.begin();
            shader.setUniformTexture("maskTex", maskFbo.getTextureReference(), 1 );
            map2.draw(0,0);
            shader.end();
            fbo.end();
            
            //map.draw(0, 0);
            ofTranslate(0, 0, 1);
            fbo.draw(0, 0);
            
        }
        else
        {
            map.draw(0, 0);
        }
        
        ofPopMatrix();
        //maskFbo.draw(0, 0, maskFbo.getWidth() * 0.4, maskFbo.getHeight() * 0.4);
    }
    
    bool hitTest(ofVec3f _p)
    {
        _p -= p;
        _p.y = map.getHeight() - _p.y;        
        if (_p.x >= 0 && _p.x < map.getWidth() && _p.y >= 0 && _p.y < map.getHeight())
        {
            int x = (int)_p.x;
            int y = (int)_p.y;
            int w = map.getWidth();
            if (pixels[(x + y * w) * 4 + 3] == 255)
            {            
                collisionPoint = _p;
                return true;
            }
        }
        return false;
    }
    
    void collide()
    {
        maskFbo.begin();
        ofEnableBlendMode(OF_BLENDMODE_ADD);
        ofSetColor(2);
        ofFill();
        for (int i = 0; i < 10; i++)
        {
            ofCircle(collisionPoint + ofVec2f(ofRandomf(), ofRandomf()) * 10, ofRandom(5, 20));
        }
        ofEnableAlphaBlending();
        maskFbo.end();
    }
    
    void setupShader()
    {
        int width = map.getWidth();
        int height = map.getHeight();
        
        maskFbo.allocate(width,height);
        fbo.allocate(width,height);
        
        string shaderProgram = "#version 120\n \
            #extension GL_ARB_texture_rectangle : enable\n \
            \
            uniform sampler2DRect tex0;\
            uniform sampler2DRect maskTex;\
            \
            void main (void){\
            vec2 pos = gl_TexCoord[0].st;\
            \
            vec3 src = texture2DRect(tex0, pos).rgb;\
            float mask = texture2DRect(maskTex, pos).r * texture2DRect(tex0, pos).a;\
            \
            gl_FragColor = vec4( src , mask);\
            }";
        shader.setupShaderFromSource(GL_FRAGMENT_SHADER, shaderProgram);
        shader.linkProgram();
        
        maskFbo.begin();
        ofClear(0,0,0,0);
        maskFbo.end();
        fbo.begin();
        ofClear(0,0,0,255);
        fbo.end();
    }
};

#endif
