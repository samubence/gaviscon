//
//  FireParticle.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_FireParticle_h
#define GameplayDemo_FireParticle_h

#include "ofMain.h"
#include "RenderListElement.h"

/*
 TASKS:
    fire fx
 */
class FireParticle : public RenderListElement
{
public:
    FireParticle(ofVec3f _p, ofTexture * _texture)
    {
        p = _p;
        texture = _texture;
        alive = true;
        interval = ofRandom(1, 2);
        
        started = ofGetElapsedTimef();
        rotation = ofRandomf() * 360;
        rotSpeed = 0;//ofRandomf() * 25;
        
        seed = ofVec2f(ofRandomf() * 100, ofRandomf() * 100);
        amp = ofVec2f(ofRandom(0.01, 0.2), ofRandom(0.06, 0.1));
    }
    
    void update()
    {
        if (ofGetElapsedTimef() - started > interval)
        {
            alive = false;
        }
        else
        {
            float tFreq = 20;
            
            float vx = ofMap(ofNoise(ofGetElapsedTimef() * tFreq, 123.456, seed.x), 0, 1, -amp.x, amp.x);
            float vy = ofMap(ofNoise(ofGetElapsedTimef() * tFreq, 987.654, seed.y), 0, 1, 0, amp.y);
            v += ofVec2f(vx, vy);
            p += v;
            
            rotation += rotSpeed;
        }
    }
    
    void draw()
    {
        ofEnableBlendMode(OF_BLENDMODE_ADD);
        float pt = ofMap(ofGetElapsedTimef(), started, started + interval, 1, 0, true);
        float size = powf(pt, 1.5) * 200;
        ofPushMatrix();
        ofTranslate(p);
        ofRotate(rotation, 0, 0, 1);
        ofSetColor(ofMap(pt, 0, 1, 100, 20));
        int n = ofMap(ofGetElapsedTimef(), started, started + interval, 0, 4 * 4);
        //n = 0;
        drawPart(texture, n, size);
        ofPopMatrix();
        ofEnableAlphaBlending();
    }
    
    void drawPart(ofTexture *texture, int n, float size)
    {
        int w = (int)texture->getWidth();
        int h = (int)texture->getHeight();
        
        int grid = 4;
        
        float s = w / (float)grid;
        float x = (n % grid) * s;
        float y = (n / grid) * s;
        
        ofRectangle dst(-size / 2., -size / 2., size, size);
        ofRectangle src(x, y, s, s);
        ofDisableNormalizedTexCoords();
        ofPushMatrix();
        texture->bind();
        glBegin(GL_TRIANGLE_STRIP);
        
        glTexCoord2f(src.x, src.y);
        glVertex2f(dst.x, dst.y);
        
        glTexCoord2f(src.x, src.y + src.height);
        glVertex2f(dst.x, dst.y + dst.height);
        
        glTexCoord2f(src.x + src.width, src.y);
        glVertex2f(dst.x + dst.width, dst.y);
        
        glTexCoord2f(src.x + src.width, src.y + src.height);
        glVertex2f(dst.x + dst.width, dst.y + dst.height);
        
        glEnd();
        texture->unbind();
        ofPopMatrix();
    }
    
    bool alive;
    ofVec3f v;
    float friction;
    ofTexture * texture;
    float started, interval;
    float rotation, rotSpeed;
    ofVec2f seed;
    ofVec2f amp;
};

#endif
