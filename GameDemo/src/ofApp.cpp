#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofLoadImage(flameTexture, "flameseq.png");
    ofLoadImage(ball, "whiteball.png");
    ofLoadImage(sausage, "sausage.png");
    
    camPos.set(0, 0, 600);
    cam.setGlobalPosition(camPos);
    gun.setup(ofVec3f(-0, -50, 500));
    
    int n = 0;
    for (int i = 0; i < n; i++)
    {
        ofVec3f p(ofRandomf() * 500, -100, -1);
        Fire * f = new Fire(p, &flameTexture);
        fire.push_back(f);
    }
    
    lLevel.setup("level.png", "level_cool.png", ofVec3f(-623.437500, -369.444458, 0), 1);
    lMid.setup("mid.png", ofVec3f(-2200, -850, -1000), 2.5);
    lBg.setup("bg.jpg", ofVec3f(-3000, -1500, -2000), 5);
    
    
    moving = false;
    
    oscReceiver.setup(12345);
    
    ofHideCursor();
    ofToggleFullscreen();
}

//--------------------------------------------------------------
void ofApp::update()
{
    while (oscReceiver.hasWaitingMessages())
    {
        ofxOscMessage m;
        oscReceiver.getNextMessage(&m);
        if (m.getAddress() == "quat")
        {
            ofQuaternion quat;
            quat.set(m.getArgAsFloat(0), m.getArgAsFloat(2), m.getArgAsFloat(1), m.getArgAsFloat(3));
            gun.setGlobalOrientation(quat);
            
        }
    }
    
    if (ofGetMousePressed())
    {
        ofVec3f cp = cam.getGlobalPosition();
        gun.lookAt(ofVec3f(ofMap(mouseX, 0, ofGetWidth(), -1000, 1000) + cp.x, ofMap(mouseY, 0, ofGetHeight(), 1000, -1000), 0));
    }
    if (ofRandom(0, 100) < 1)
        addObject();
    
    updateObjects();
    updateFire();
    updateWater();
    
    renderList.clear();
    /*
    for (int i = 0; i < firePts.size(); i++)
    {
        RenderListElement * e = firePts[i];
        renderList.add(e);
    }
     */
    for (int i = 0; i < waterPts.size(); i++)
    {
        RenderListElement * e = waterPts[i];
        renderList.add(e);
    }
    for (int i = 0; i < objs.size(); i++)
    {
        RenderListElement * e = objs[i];
        renderList.add(e);
    }
    
    renderList.add(&lLevel);
    renderList.add(&lMid);
    renderList.add(&lBg);
    
    renderList.add(&gun);

    if (moving)
    {
        camPos.x += 1;
        
        
    }
    
    float f = 0.1;
    float nx = (ofNoise(123.456, ofGetElapsedTimef() * f) - 0.5) * 50;
    float ny = (ofNoise(987.654, ofGetElapsedTimef() * f) - 0.5) * 50;
    cam.setGlobalPosition(camPos + ofVec2f(nx, ny));
    ofVec3f cp = cam.getGlobalPosition();
    gun.setGlobalPosition(cp.x, cp.y -50, 500);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(0);
    
    cam.begin();
    
    ofEnableDepthTest();
    
    renderList.draw();
    
    //gun.draw();
    
    ofDisableDepthTest();
    
    for (int i = 0; i < firePts.size(); i++)
    {
        firePts[i]->draw();
    }
    
    for (int i = 0; i < fire.size(); i++)
    {
        //fire[i]->draw();
    }
    
    cam.end();
    
    ofDrawBitmapStringHighlight(ofToString((int)ofGetFrameRate()), 20, 20);
}

void ofApp::addObject()
{
    ofVec3f camPos = cam.getGlobalPosition();
    
    ofVec3f p(camPos.x + ofRandom(-500, 500), 500, 9);
    Object * o = new Object(p, &sausage);
    objs.push_back(o);
}

void ofApp::addWater()
{
    float speed = 10;
    ofVec3f p(gun.getGlobalPosition() + gun.getDirection() * 20);
    ofVec3f v(gun.getDirection() * speed);
    WaterParticle * w = new WaterParticle(p, v, &ball);
    waterPts.push_back(w);
}

void ofApp::updateObjects()
{
    for (vector<Object*>::iterator i = objs.begin(); i != objs.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update();
            if (lLevel.hitTest((*i)->p))
            {
                (*i)->alive = false;
                fire.push_back(new Fire((*i)->p + ofVec3f(0, 0, 10), &flameTexture));
            }
            i++;
        }
        else
        {
            delete *i;
            i = objs.erase(i);
        }
    }
}

void ofApp::updateFire()
{
     
    
    for (vector<FireParticle*>::iterator i = firePts.begin(); i != firePts.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update();
            i++;
        }
        else
        {
            delete *i;
            i = firePts.erase(i);
        }
    }
}

void ofApp::updateWater()
{
    addWater();
    
    for (vector<WaterParticle*>::iterator i = waterPts.begin(); i != waterPts.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update();
            
            if ((*i)->p.z <= 0 && (*i)->p.z > -30)
            {
                if (lLevel.hitTest((*i)->p))
                {
                    lLevel.collide();
                }
            }
            
            i++;
        }
        else
        {
            delete *i;
            i = waterPts.erase(i);
        }
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == 'f')
    {
        ofToggleFullscreen();
    }
    if (key == 'm') moving = !moving;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
