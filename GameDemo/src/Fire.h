//
//  Fire.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Fire_h
#define GameplayDemo_Fire_h

#include "ofMain.h"
#include "FireParticle.h"
#include "WaterParticle.h"
#include "Fire.h"
#include "RenderListElement.h"

/*
 TASKS:
    generated on Object collision
    emits fire particles
    collide with water
*/

class Fire : public RenderListElement
{
public:
    Fire(ofVec3f _p, ofTexture * _texture)
    {
        p = _p;
        alive = true;
        life = lifeMax = 10;
        hitArea = 50;
        texture = _texture;
    }
    
    void update(vector<FireParticle*> * firePts, vector<WaterParticle*> * waterPts, vector<Fire*> * fire)
    {
        int n = (int)ofMap(life, lifeMax, 0, 2, 1);
        float s = ofMap(life, lifeMax, 0, 10, 0);
        
        for (int i = 0; i < n; i++)
        {
            if (ofRandom(0, 100) < 20)
            {
            ofVec3f rnd(ofRandomf(), ofRandomf(), ofRandomf());
            firePts->push_back(new FireParticle(p + rnd * s, texture));
            }
        }
        
        for (vector<WaterParticle*>::iterator i = waterPts->begin(); i != waterPts->end(); i++)
        {
            float d = (*i)->p.distance(p);
            if (d < hitArea)
            {
                (*i)->alive = false;
                life --;
                if (life < 0)
                {
                    alive = false;
                }
                
            }
        }
    }
    
    void draw()
    {
        ofSetColor(0, 20);
        ofNoFill();
        ofCircle(p, hitArea);
        
        ofNoFill();
        ofSetColor(0);
        ofRect(p.x - 20, p.y - hitArea, 40, 2);
        
        float ptc = ofMap(life, 0, lifeMax, 0, 1);
        ofFill();
        ofSetColor(255);
        ofRect(p.x - 20, p.y - hitArea, 40 * ptc, 2);
    }
    
    bool alive;
    int life, lifeMax;
    float hitArea;
    ofTexture * texture;
};

#endif
