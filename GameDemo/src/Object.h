//
//  Object.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Object_h
#define GameplayDemo_Object_h

#include "ofMain.h"
#include "Fire.h"
#include "RenderListElement.h"

/*
 TASKS:
    generated on top of the screen
    falling
    when hitting terrian or ther Object geneartes Fire object
 */
class Object : public RenderListElement
{
public:
    Object(ofVec3f _p, ofTexture * _texture)
    {
        p = _p;
        texture = _texture;
        alive = true;
        hitArea = 50;
        rot = ofRandom(0, 360);
        rotSpeed = ofRandom(2, 10);
        if (ofRandomf() < 0) rotSpeed *= -1;
    }
    
    void update()
    {
        float g = -0.1;
        v += ofVec3f(0, g, 0);
        p += v;
        /*
        for (int i = 0; i < fire->size(); i++)
        {
            float d = (*fire)[i]->p.distance(p);
            if (d < hitArea)
            {
                alive = false;
                fire->push_back(new Fire(p, texture));
                break;
            }
        }
        */
        if (p.y < -1000)
        {
            alive = false;
            //fire->push_back(new Fire(p, texture));
        }
        
        rot += rotSpeed;
    }
    
    void draw()
    {
        float w = 80;
        float h = texture->getHeight() / texture->getWidth() * w;
        ofSetColor(255);
        ofPushMatrix();
        ofTranslate(p);
        ofRotate(rot, 0, 0, 1);
        //ofRect(p.x - w / 2., p.y - h / 2., w, h);
        texture->draw(- w / 2., - h / 2., w, h);
        ofPopMatrix();
    }
    
    bool alive;
    ofVec3f v;
    float hitArea;
    ofTexture * texture;
    float rot, rotSpeed;
};

#endif
