#include "ofApp.h"
#include "ofxXmlSettings.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofxXmlSettings pxml;

    pxml.load("ver.xml");
    pxml.pushTag("version");
    int pVer = pxml.getValue("ver", -1);
    pxml.popTag();

    printf("prev ver: %i\n", pVer);
    ofHttpResponse r = ofLoadURL("http://www.binaura.net/bnc/temp/gaviscon/ver.dat");

    ofxXmlSettings nxml;
    if (nxml.loadFromBuffer(r.data))
    {

        nxml.pushTag("version");
        int ver = nxml.getValue("ver", -1);
        nxml.popTag();
        printf("new ver: %i\n", ver);
        if (ver > pVer)
        {

            ofxXmlSettings _xml;
            int vTag = _xml.addTag("version");
            _xml.pushTag("version", vTag);
            _xml.addValue("ver", ver);
            _xml.popTag();
            _xml.save("ver.xml");

            ofHttpResponse response = ofLoadURL("http://www.binaura.net/bnc/temp/gaviscon/bin.zip");
            ofBuffer b = response.data;
            if (b.size() > 0)
            {
                if (ofBufferToFile("bin.zip", b, true))
                {
                    string path = ofToDataPath("", true);
                    path = path.substr(0, path.length() - 5);
                    string unzipStr = "7z x " + path + "data/bin.zip" + " -o" + path + " -y";
                    printf(unzipStr.c_str());
                    ofSystem(unzipStr);
                }
                else
                {
                    printf("cannot save file...\n");
                }
            }
        }
    }
    OF_EXIT_APP(0);
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
