package bnc.binaura.gaviscongun;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import oscP5.*;
import netP5.*;


public class GavisconGun extends Activity implements SensorEventListener {
    public static final String PREFS_NAME = "MyPrefsFile";
    SharedPreferences settings;

    SensorManager sensorManager;
    String ipStr;
    Handler mHandler;
    int mInterval = 1000 / 30;

    OscP5 osc;
    NetAddress remoteLocation;

    float[] rMatrix = new float[9];
    float[] euler = new float[3];

    boolean isLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gaviscon_gun);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        settings = getSharedPreferences(PREFS_NAME, 0);
        ipStr = settings.getString("ip", "192.168.0.100");
        isLeft = settings.getBoolean("isLeft", true);

        final EditText ipInput = (EditText)findViewById(R.id.ipinput);
        ipInput.setText(ipStr);

        ipInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                SharedPreferences.Editor editor = settings.edit();

                editor.putString("ip", charSequence.toString());
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Button startBtn = (Button)findViewById(R.id.button);
        startBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Log.d("bnc", "START");
                try {
                    String ipStr = ipInput.getText().toString();

                    remoteLocation = new NetAddress(ipStr, 12345);
                }
                catch (Exception e)
                {
                    Log.d("bnc", "Error: " + e);
                }

                EditText ipInput = (EditText)findViewById(R.id.ipinput);
                ipInput.setVisibility(View.GONE);
                Button startBtn = (Button)findViewById(R.id.button);
                startBtn.setVisibility(View.GONE);
                ToggleButton toggleButton = (ToggleButton)findViewById(R.id.toggleButton);
                toggleButton.setVisibility(View.GONE);
                TextView tv = (TextView)findViewById(R.id.textView);
                tv.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ipInput.getWindowToken(), 0);

                updateThread.run();
                return false;
            }
        });

        ToggleButton toggleButton = (ToggleButton)findViewById(R.id.toggleButton);
        toggleButton.setChecked(!isLeft);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isLeft = !isChecked;
                SharedPreferences.Editor editor = settings.edit();

                editor.putBoolean("isLeft", isLeft);
                editor.commit();
            }
        });

        mHandler = new Handler();

        sensorManager = (SensorManager)getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
        Sensor s = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (s != null)
        {
            sensorManager.registerListener(this, s, SensorManager.SENSOR_DELAY_GAME);
        }

    }

    Runnable updateThread = new Runnable() {
        @Override
        public void run() {
            update();
            mHandler.postDelayed(updateThread, mInterval);
        }
    };

    public void update()
    {
        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids) {
                String addr = "/e" + (isLeft ? "0" : "1");
                OscMessage msg = new OscMessage(addr);
                msg.add(euler[0]);
                msg.add(euler[1]);
                msg.add(euler[2]);

                OscP5.flush(msg, remoteLocation);
                //Log.d("bnc", euler[0] + " " + euler[1] + " " + euler[2] + "  ::  " + hEuler[0] + " " + hEuler[1] + " " + hEuler[2]);
                return null;
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gaviscon_gun, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mHandler.removeCallbacks(updateThread);
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {

            calculateAngles(euler, sensorEvent.values);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void calculateAngles(float[] result, float[] rVector) {

        // caculate rotation matrix from rotation vector first
        SensorManager.getRotationMatrixFromVector(rMatrix, rVector);

        // calculate Euler angles now
        SensorManager.getOrientation(rMatrix, result);

        // The results are in radians, need to convert it to degrees

        convertToDegrees(result);
    }


    private void convertToDegrees(float[] vector) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = (float)Math.toDegrees(vector[i]);
        }
    }
}
