package bnc.binaura.gavisconcontrol;

/**
 * Created by bnc on 23/08/14.
 */
public interface GetUrlListener {
    public void onGetUrlEvent(String response);
}
