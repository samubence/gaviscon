package bnc.binaura.gavisconcontrol;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GavisconControl extends Activity implements GetUrlListener, DownloadImageListener {

    public static final String PREFS_NAME = "MyPrefsFile";
    SharedPreferences settings;

    String ipAddr, preAddr, postAddr;

    boolean imageDownloaded;

    Button btnStart, btnDownload, btnSendMail1, btnSendMail2, btnHome, btnLang;
    EditText edtFullName1, edtNickName1, edtFullName2, edtNickName2, edtMail1, edtMail2, edtIp;
    ImageView imageView;
    TextView tvSystemSettings;

    String root, dirPath, fileName;

    int lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.controller);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        btnStart = (Button)findViewById(R.id.startButton);
        btnDownload = (Button)findViewById(R.id.downloadButton);
        btnSendMail1 = (Button)findViewById(R.id.user1SendEmail);
        btnSendMail2 = (Button)findViewById(R.id.user2SendEmail);
        btnHome = (Button)findViewById(R.id.homeButton);
        btnLang = (Button)findViewById(R.id.langButton);
        edtFullName1 = (EditText)findViewById(R.id.user1Name);
        edtFullName2 = (EditText)findViewById(R.id.user2Name);
        edtNickName1 = (EditText)findViewById(R.id.user1Nick);
        edtNickName2 = (EditText)findViewById(R.id.user2Nick);
        edtMail1 = (EditText)findViewById(R.id.user1Email);
        edtMail2 = (EditText)findViewById(R.id.user2Email);
        edtIp = (EditText)findViewById(R.id.ipInput);
        imageView = (ImageView)findViewById(R.id.imageView);
        tvSystemSettings = (TextView)findViewById(R.id.textSystemSettings);

        preAddr = "http://";
        postAddr = ":8080/";

        settings = getSharedPreferences(PREFS_NAME, 0);
        ipAddr = settings.getString("ip", "192.168.0.100");
        lang = settings.getInt("lang", 0);

        updateLanguage();

        edtIp.setText(ipAddr);

        root = Environment.getExternalStorageDirectory().toString();
        dirPath = "gaviscon";
        fileName = "gaviscon.jpg";
        imageDownloaded = false;

        final GavisconControl app = this;

        edtMail1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                openPopup1();
                return false;
            }
        });

        edtMail2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                openPopup2();
                return false;
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String parameters = "";

                if (edtNickName1.getText().toString().equals("") == false) parameters = Uri.encode(edtNickName1.getText().toString());
                if (edtNickName2.getText().toString().equals("") == false)
                {
                    if (parameters == "") parameters = Uri.encode(edtNickName2.getText().toString());
                    else parameters += "," + Uri.encode(edtNickName2.getText().toString());
                }
                if (parameters.length() > 0) {
                    //Log.d("bnc", preAddr + ipAddr + postAddr + "start," + parameters);
                    new GetUrlTask(app, preAddr + ipAddr + postAddr + "start," + parameters).execute();
                    imageView.setImageBitmap(null);
                    imageDownloaded = false;
                    updateLanguage();
                }
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageDownloaded == false) {
                    new GetUrlTask(app, preAddr + ipAddr + postAddr + "getCurrentImage").execute();
                }
            }
        });

        btnSendMail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageDownloaded) {
                    if (edtMail1.getText().length() > 0 && edtFullName1.getText().length() > 0) {
                        sendEmail(edtMail1.getText().toString(), edtFullName1.getText().toString(), root + "/" + dirPath + "/" + fileName);
                    }
                }
            }
        });

        btnSendMail2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageDownloaded) {

                    if (edtMail2.getText().length() > 0 && edtFullName2.getText().length() > 0) {
                        sendEmail(edtMail2.getText().toString(), edtFullName2.getText().toString(), root + "/" + dirPath + "/" + fileName);
                    }
                }
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                new GetUrlTask(app, preAddr + ipAddr + postAddr + "home").execute();
                edtFullName1.setText("");
                edtFullName2.setText("");
                edtNickName1.setText("");
                edtNickName2.setText("");
                edtMail1.setText("");
                edtMail2.setText("");
                imageView.setImageBitmap(null);
            }
        });

        btnLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CharSequence[] items = {
                        "Magyar", "Czech"
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(app);
                builder.setTitle("Select your language");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        lang = item;
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("lang", lang);
                        editor.commit();
                        new GetUrlTask(app, preAddr + ipAddr + postAddr + "lang," + lang).execute();
                        updateLanguage();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        edtIp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                ipAddr = charSequence.toString();

                SharedPreferences.Editor editor = settings.edit();
                editor.putString("ip", ipAddr);
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void sendEmail(String email, String name, String file)
    {
        Log.d("bnc", email + "," + name + "," + file);
        String[] emailAddrs = new String[1];
        emailAddrs[0] = email;

        Uri fileToAttach= Uri.parse("file://" + file);
        String htmlText = Texts.EMAIL_TEXT_DEAR[lang] + " " + name + Texts.EMAIL_TEXT_DEAR_END[lang] + "<br><br>" + Texts.EMAIL_TEXT_CONTENT[lang];

        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setType("text/html");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, emailAddrs);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, Texts.EMAIL_SUBJECT[lang]);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(htmlText));
        emailIntent.putExtra(Intent.EXTRA_STREAM, fileToAttach);
        //emailIntent.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
        emailIntent.setClassName("com.android.email", "com.android.email.activity.MessageCompose");
        this.startActivity(Intent.createChooser(emailIntent, "Sending email..."));
    }

    @Override
    public void onGetUrlEvent(String response) {
        Log.d("bnc", "response: " + response);
        if (response.contains(".jpg"))
        {
            new DownloadImageTask(this, preAddr + ipAddr + postAddr + response).execute();
        }
    }

    @Override
    public void onDownloadImageEvent(Bitmap bitmap) {

        if (bitmap != null) {
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            imageView.setImageBitmap(bitmap);
            saveBitmap(bitmap, root + "/" + dirPath, fileName, 90);
            imageDownloaded = true;
            updateLanguage();
        }
    }

    public void saveBitmap(Bitmap bitmap, String root, String fileName, int compression)
    {
        File myDir = new File(root);
        myDir.mkdirs();
        File file = new File (myDir, fileName);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, compression, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateLanguage()
    {
        edtFullName1.setHint(Texts.NAMELEFT[lang]);
        edtFullName2.setHint(Texts.NAMERIGHT[lang]);
        edtNickName1.setHint(Texts.NICKLEFT[lang]);
        edtNickName2.setHint(Texts.NICKRIGHT[lang]);
        edtMail1.setHint(Texts.EMAILLEFT[lang]);
        edtMail2.setHint(Texts.EMAILRIGHT[lang]);
        btnStart.setText(Texts.START[lang]);
        btnSendMail1.setText(Texts.LEFTSENDMAIL[lang]);
        btnSendMail2.setText(Texts.RIGHTSENDMAIL[lang]);
        btnDownload.setText(Texts.DOWNLOAD[lang]);
        btnHome.setText(Texts.HOME[lang]);
        tvSystemSettings.setText(Texts.SYSTEMSETTINGS[lang]);
        btnLang.setText(Texts.LANGUAGES[lang]);
    }

    public void openPopup1()
    {
        final GavisconControl app = this;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Texts.AGREEMENT_TEXT[lang]).setTitle("");

        CheckBox cb = new CheckBox(this);
        cb.setPadding(0, 100, 0, 100);
        builder.setView(cb);

        builder.setPositiveButton(Texts.AGREEMENT_AGGRE[lang], new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm");
                String currentDateandTime = sdf.format(new Date());
                String toStore = Uri.encode(edtMail1.getText().toString()) + "," + Uri.encode(edtFullName1.getText().toString()) + "," + currentDateandTime;
                new GetUrlTask(app, preAddr + ipAddr + postAddr + "store," + toStore).execute();

            }
        });
        builder.setNegativeButton(Texts.AGREEMENT_CANCEL[lang], new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void openPopup2()
    {
        final GavisconControl app = this;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Texts.AGREEMENT_TEXT[lang]).setTitle("");

        CheckBox cb = new CheckBox(this);
        cb.setPadding(0, 100, 0, 100);
        builder.setView(cb);

        builder.setPositiveButton(Texts.AGREEMENT_AGGRE[lang], new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm");
                String currentDateandTime = sdf.format(new Date());
                String toStore = Uri.encode(edtMail2.getText().toString()) + "," + Uri.encode(edtFullName2.getText().toString()) + "," + currentDateandTime;
                new GetUrlTask(app, preAddr + ipAddr + postAddr + "store," + toStore).execute();

            }
        });
        builder.setNegativeButton(Texts.AGREEMENT_CANCEL[lang], new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gaviscon_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
