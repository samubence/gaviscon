package bnc.binaura.gavisconcontrol;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Created by bnc on 23/08/14.
 */
public class DownloadImageTask extends AsyncTask<Void, Void, Bitmap> {
    DownloadImageListener listener;
    String urlStr;
    public DownloadImageTask(DownloadImageListener _listener, String _urlStr) {
        listener = _listener;
        urlStr = _urlStr;
    }

    protected Bitmap doInBackground(Void... params) {
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(urlStr).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap result) {
        listener.onDownloadImageEvent(result);
    }
}
