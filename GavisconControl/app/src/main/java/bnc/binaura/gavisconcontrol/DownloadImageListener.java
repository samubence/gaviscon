package bnc.binaura.gavisconcontrol;

import android.graphics.Bitmap;

/**
 * Created by bnc on 23/08/14.
 */
public interface DownloadImageListener {

    public void onDownloadImageEvent(Bitmap bitmap);
}
