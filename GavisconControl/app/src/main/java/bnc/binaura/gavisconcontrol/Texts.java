package bnc.binaura.gavisconcontrol;

import java.util.ArrayList;

/**
 * Created by bnc on 23/08/14.
 */
public class Texts {
    static String[] NAMELEFT = new String[]{"1. név", "1. Jméno"};
    static String[] NAMERIGHT = new String[]{"2. név", "2. Jméno"};
    static String[] NICKLEFT = new String[]{"1. becenév", "1. přesdívka"};
    static String[] NICKRIGHT = new String[]{"2. becenév", "2. přesdívka"};
    static String[] EMAILLEFT = new String[]{"1. email cím", "1. E-mailová adresa"};
    static String[] EMAILRIGHT = new String[]{"2. email cím", "2. E-mailová adresa"};
    static String[] START = new String[]{"játék indítása", "spustit hru"};
    static String[] DOWNLOAD = new String[]{"kép letöltés", "stáhnout fotku"};
    static String[] LEFTSENDMAIL = new String[]{"1. email küldés", "1. Poslat e-mail"};
    static String[] RIGHTSENDMAIL = new String[]{"2. email küldés", "2. Poslat e-mail"};
    static String[] HOME = new String[]{"kezdőképernyő", "demo verze"};
    static String[] SYSTEMSETTINGS = new String[]{"rendszer beállítások", "Nastavení"};
    static String[] LANGUAGES = new String[]{"nyelv választás / vybrat jazyk", "nyelv választás / vybrat jazyk"};
    static String[] EMAIL_SUBJECT = new String[]{"Gaviscon", "Gaviscon"};
    static String[] EMAIL_TEXT_DEAR = new String[]{"Kedves", "Dobrý den"};
    static String[] EMAIL_TEXT_DEAR_END = new String[]{" !", " ,"};
    static String[] EMAIL_TEXT_CONTENT = new String[]{
            "Mivel hősiesen küzdöttél a gyomorsavval, így a játék során készült fotóiddal jutalmazunk.<br>"+
            "Gratulálunk az elért eredményhez, és bízunk benne, hogy a jövőben is mindig a Gaviscon hatékonyságával szállsz harcba a gyomorégés ellen!<br><br>"+
            "Üdvözlettel,<br>Gaviscon<br><br><br><br>"+
            "Kijelentem, hogy a játékban való részvétellel önkéntesen és kifejezetten hozzájárultam ahhoz, hogy a jelen regisztráció során megadott személyes adataimat a Reckitt Benckiser Kft., mint adatkezelő (a továbbiakban: Adatkezelő) a játékkal kapcsolatos fotóküldés céljából, továbbá a játék lebonyolítását követően piackutatás és közvetlen üzletszerzés (marketing, direkt marketing, fogyasztói szokások felmérésére szolgáló adatbázis építés, hírlevél küldése céljából kezelt adatbázis, adatbázis építése) céljából kezelje és tárolja, és a megadott e-mail címemre marketing tartalmú hírlevelet küldjön. Adatkezelő adatvédelmi nyilatkozata itt olvasható: http://legal.reckittbenckiser.com/HU/PrivacyStatement.htm",

            "Protože jste se účastnil hry Gaviscon hrdina při pálení žáhy, posíláme vám fotky, které byly během ní pořízeny. Gratulujeme ještě jednou k výsledkům.<br>Věříme, že v případě pálení žáhy nebo poruchy trávení sáhnete sáhnete právě pro účinnou úlevu od Gavisconu.<br><br>Pozdravy,<br><br>Gaviscon<br><br><br>" +
            "Souhlas účastníka marketingové a/nebo spotřebitelské soutěže a akce společnosti Reckitt Benckiser (Czech Republic), spol. s r.o. se zpracováváním osobních údajů<br><br>" +
            "Tímto výslovně souhlasím s tím, aby společnost Reckitt Benckiser (Czech Republic), spol. s r.o. (dálejen „RB CZ“), člen nadnárodní skupiny společností Reckitt Benckiser (dále jen „společnosti RB<br>“), v souvislosti s mou účastí v marketingové a/nebo spotřebitelské soutěži a akci shromažďovala, zpracovávala a uchovávala některé údaje vztahující se k mé osobě v rozsahu (jméno a příjmení, emailová adresa a případně fotografie), a to způsobem a za podmínek uvedených v Informaci o nakládání s osobními údaji spotřebitelů a účastníků marketingových akcí společnosti Reckitt Benckiser (Czech Republic), spol. s r.o http://legal.reckittbenckiser.com/CZ/TermsandConditions.htm (dále jen „Informace“), a pro účely tam uvedené, zejména pro účely reklamy a marketingu, včetně zasílání obchodních sdělení.<br>Fotografie mé osoby bude společností RB CZ zpracovávána pouze, pokud jsem společnost RB CZ v souvislosti s mou účastí na marketingové akci požádal(a) o zaslání pořízené fotografie na mou emailovou adresu. Po zaslání fotografie bude tato společností RB CZ smazána, tj. společnost RB CZ nebude fotografii nadále zpracovávat či uchovávat. Společnost RB CZ není oprávněna pořízenou fotografii šířit či jinak zpřístupňovat veřejnosti<br>Rovněž souhlasím s tím, že společnost RB CZ bude předávat za účely uvedenými v Informaci některé mé poskytnuté osobní údaje (v rozsahu: jméno, příjmení, emailová adresa) do zahraničí, kdy tyto osobní údaje budou uchovávány ve Velké Británii a následně zpřístupňovány dalším společnostem skupiny společností RB, dle aktuálního složení skupiny společností RB.<br>Prohlašuji, že mi je nejméně 18 let a výslovně souhlasím, aby společnost RB CZ mé osobní údaje za shora uvedených podmínek zpracovávala.<br>"+
            "Tento souhlas uděluji společnosti RB CZ na dobu trvání marketingové a/nebo spotřebitelské soutěže a akce a dále na dobu dvou let po jejím ukončení, nevyplývá-li z právních předpisů doba delší.<br>"+
            "Prohlašuji, že mi je nejméně 18 let a výslovně souhlasím, aby společnost RB CZ mé osobní údaje za shora uvedených podmínek zpracovávala."};

    static String[] AGREEMENT_TEXT = new String[]{"Kijelentem, hogy a játékban való részvétellel önkéntesen és kifejezetten hozzájárultam ahhoz, hogy a jelen regisztráció során megadott személyes adataimat a Reckitt Benckiser Kft., mint adatkezelő (a továbbiakban: Adatkezelő) a játékkal kapcsolatos fotóküldés céljából, továbbá a játék lebonyolítását követően piackutatás és közvetlen üzletszerzés (marketing, direkt marketing, fogyasztói szokások felmérésére szolgáló adatbázis építés, hírlevél küldése céljából kezelt adatbázis, adatbázis építése) céljából kezelje és tárolja, és a megadott e-mail címemre marketing tartalmú hírlevelet küldjön. Adatkezelő adatvédelmi nyilatkozata itt olvasható: http://legal.reckittbenckiser.com/HU/PrivacyStatement.htm\n" +
            "\n" +
            "Elmúltam 18 éves",

            "Souhlas účastníka marketingové a/nebo spotřebitelské soutěže a akce společnosti Reckitt Benckiser (Czech Republic), spol. s r.o. se zpracováváním osobních údajů\n"+
            "\n" +
            "Tímto výslovně souhlasím s tím, aby společnost Reckitt Benckiser (Czech Republic), spol. s r.o. (dálejen „RB CZ“), člen nadnárodní skupiny společností Reckitt Benckiser (dále jen „společnosti RB“), v souvislosti s mou účastí v marketingové a/nebo spotřebitelské soutěži a akci shromažďovala, zpracovávala a uchovávala některé údaje vztahující se k mé osobě v rozsahu (jméno a příjmení, emailová adresa a případně fotografie), a to způsobem a za podmínek uvedených v Informaci o nakládání s osobními údaji spotřebitelů a účastníků marketingových akcí společnosti Reckitt Benckiser (Czech Republic), spol. s r.o http://legal.reckittbenckiser.com/CZ/TermsandConditions.htm (dále jen „Informace“), a pro účely tam uvedené, zejména pro účely reklamy a marketingu, včetně zasílání obchodních sdělení. \n" +
            "\n" +
            "Fotografie mé osoby bude společností RB CZ zpracovávána pouze, pokud jsem společnost RB CZ v souvislosti s mou účastí na marketingové akci požádal(a) o zaslání pořízené fotografie na mou emailovou adresu. Po zaslání fotografie bude tato společností RB CZ smazána, tj. společnost RB CZ nebude fotografii nadále zpracovávat či uchovávat. Společnost RB CZ není oprávněna pořízenou fotografii šířit či jinak zpřístupňovat veřejnosti\n" +
            "\n" +
            "Rovněž souhlasím s tím, že společnost RB CZ bude předávat za účely uvedenými v Informaci některé mé poskytnuté osobní údaje (v rozsahu: jméno, příjmení, emailová adresa) do zahraničí, kdy tyto osobní údaje budou uchovávány ve Velké Británii a následně zpřístupňovány dalším společnostem skupiny společností RB, dle aktuálního složení skupiny společností RB.\n" +
            "\n" +
            "Tento souhlas uděluji společnosti RB CZ na dobu trvání marketingové a/nebo spotřebitelské soutěže a akce a dále na dobu dvou let po jejím ukončení, nevyplývá-li z právních předpisů doba delší.\n" +
            "\n" +
            "Prohlašuji, že mi je nejméně 18 let a výslovně souhlasím, aby společnost RB CZ mé osobní údaje za shora uvedených podmínek zpracovávala."};
    static String[] AGREEMENT_AGGRE = new String[] {"Elfogadom", "Souhlasím"};
    static String[] AGREEMENT_CANCEL = new String[] {"", ""};

}
