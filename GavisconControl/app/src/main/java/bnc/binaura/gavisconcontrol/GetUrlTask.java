package bnc.binaura.gavisconcontrol;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by bnc on 23/08/14.
 */
public class GetUrlTask extends AsyncTask<Void, Void, String>{
    GetUrlListener listener;
    String urlStr;
    public GetUrlTask(GetUrlListener _listener, String _urlStr)
    {
        listener = _listener;
        urlStr = _urlStr;
        Log.d("bnc", "GetUrl: " + urlStr);
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpGet httpGet = new HttpGet();
        HttpClient httpClient = new DefaultHttpClient();
        String response = "";
        try {
            httpGet.setURI(new URI(urlStr));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            response = EntityUtils.toString(httpResponse.getEntity());
        } catch (IOException e) {
            response = "error";
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result)
    {
        listener.onGetUrlEvent(result);
    }
}
