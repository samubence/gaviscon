//
//  Gun.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Gun_h
#define GameplayDemo_Gun_h

#include "ofMain.h"
#include "RenderListElement.h"
#include "ofx3DModelLoader.h"
#include "Parameters.h"

/*
 TASKS:
    aim to cursor position
    get direction to shoot water particles
*/

class Gun : public ofNode, public RenderListElement
{
public:

    void setup()
    {
        ofSetSmoothLighting(true);
        eulerDamp = 0.5;
        cylinder.set(10, 1);
        cylinder.rotate(90, 1, 0, 0);
        cylinder.setResolution(10, 2);
    }

    void update(ofVec3f _p)
    {
        p = _p;
        setGlobalPosition(p);
        
        euler += angleDamp(euler, tEuler, eulerDamp, 180);

        setGlobalOrientation(ofQuaternion());
        rotate((-euler.x - (base.x + baseShift.x)), 0, 1, 0);
        rotate((-euler.y - (base.y + baseShift.y)), 1, 0, 0);
    }
    
    void setEuler(ofVec3f e)
    {
        tEuler = e * ofVec3f(Parameters::motionScaleX, Parameters::motionScaleY, 1);
    }
    
    void reset()
    {
        base = -tEuler;
    }

    ofVec3f getDirection()
    {
        float n = 0.05;
        return getLookAtDir() + ofVec3f(ofRandom(-n, n), ofRandom(-n, n), 0);
    }

    void draw()
    {
        transformGL();
        /*
        ofSetColor(200);
        ofFill();
        ofDrawBox(15, 15, 40);
        ofSetColor(150);
        ofDrawBox(0, 0, -20, 17, 17, 5);
         
        ofRotate(90, 0, 0, 1);
        ofRotate(90, 0, 1, 0);
*/
        ofSetColor(150);
        ofFill();
        
        ofPushMatrix();
        ofScale(1, 1, 50);
        cylinder.draw();
        ofPopMatrix();
        
        
        ofSetColor(200);
        ofFill();
        
        ofPushMatrix();
        ofTranslate(0, 0, -20);
        ofScale(1.1, 1.1, 10);
        cylinder.draw();
        ofPopMatrix();
        
        
        restoreTransformGL();
    }
    
    ofVec3f angleDamp(ofVec3f a, ofVec3f b, float damp, float pi = PI)
    {
        a = ofVec3f(fmodf(a.x, pi * 2), fmodf(a.y, pi * 2), fmodf(a.z, pi * 2));
        b = ofVec3f(fmodf(b.x, pi * 2), fmodf(b.y, pi * 2), fmodf(b.z, pi * 2));
        
        ofVec3f ba = b - a;
        
        if (ba.x < -pi)
        {
            ba.x += pi * 2;
        }
        else if (pi < ba.x)
        {
            ba.x -= pi * 2;
        }
        
        if (ba.y < -pi)
        {
            ba.y += pi * 2;
        }
        else if (pi < ba.y)
        {
            ba.y -= pi * 2;
        }
        
        if (ba.z < -pi)
        {
            ba.z += pi * 2;
        }
        else if (pi < ba.z)
        {
            ba.z -= pi * 2;
        }
        
        return ba * damp;
    }
    
    ofVec3f euler, tEuler;
    float eulerDamp;
    ofCylinderPrimitive cylinder;
    ofVec3f base, baseShift;
};


#endif
