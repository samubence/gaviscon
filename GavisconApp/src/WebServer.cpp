#include "WebServer.h"


WebServerListener* WebServer::listener;

//------------------------------------------------------------------------------
WebServerRouteHandler::Settings::Settings() {
    defaultIndex = "index.html";
    documentRoot = "DocumentRoot";
    route.path   = "/.*"; // regex
}

//------------------------------------------------------------------------------
WebServerRouteHandler::WebServerRouteHandler(const Settings& _settings) : settings(_settings) { }

//------------------------------------------------------------------------------
WebServerRouteHandler::~WebServerRouteHandler() { }

//------------------------------------------------------------------------------
void WebServerRouteHandler::handleRequest(HTTPServerRequest& request, HTTPServerResponse& response) {

    if(isValidRequest(settings.route, request, response))
    {
        if (WebServer::listener)
            WebServer::listener->handleRequest(request, response);
        else
            response.sendBuffer("", 0);
    }
}

//------------------------------------------------------------------------------
void WebServerRouteHandler::sendErrorResponse(HTTPServerResponse& response) {
    // now check to see if the status was set something other than 200 by an exception

    HTTPResponse::HTTPStatus responseStatus = response.getStatus();
    // see if we have an html file with that error code
    ofFile errorFile(settings.documentRoot + "/" + ofToString(responseStatus) + ".html");
    if(errorFile.exists()) {
        try {
            response.sendFile(errorFile.getAbsolutePath(),"text/html");
        } catch(const FileNotFoundException& exc) {
            ofxWebServerBaseRouteHandler::sendErrorResponse(response);
        } catch(const OpenFileException& exc) {
            ofxWebServerBaseRouteHandler::sendErrorResponse(response);
        }
    } else {
        // we didn't have one in the DocumentRoot, so generate one
        ofxWebServerBaseRouteHandler::sendErrorResponse(response);
    }
}

