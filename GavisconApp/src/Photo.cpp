#include "Photo.h"
#include "ofxXmlSettings.h"

Photo::Photo()
{

}

Photo::~Photo()
{

}

void Photo::setup()
{
    photoWidth = 640;
    photoHeight = 480;

    ofLoadImage(fg, "gfx/photo/fg.png");

    ofFbo::Settings s;
    s.width = fg.getWidth();
    s.height = fg.getHeight();
    s.numSamples = ofFbo::maxSamples();
    s.internalformat = GL_RGB;

    comp.allocate(s);
    comp.begin();
    ofClear(ofRandom(0, 255), ofRandom(0, 255), ofRandom(0, 255));
    comp.end();

    compView.set(0, 0, comp.getWidth(), comp.getHeight());

    ofxXmlSettings xml;
    xml.load("photocomp.xml");
    xml.pushTag("photocomp");
    int n = xml.getNumTags("photo");
    for (int i = 0; i < n; i++)
    {
        xml.pushTag("photo", i);
        ofVec4f p(xml.getValue("x", 0.), xml.getValue("y", 0.), xml.getValue("z", 0.), xml.getValue("w", 1.));
        photoPos.push_back(p);
        xml.popTag();
    }
    xml.popTag();

    vector<ofVideoDevice> devices = vGrabber.listDevices();

    for(int i = 0; i < devices.size(); i++){
		cout << devices[i].id << ": " << devices[i].deviceName;
        if( devices[i].bAvailable ){
            cout << endl;

        }else{
            cout << " - unavailable " << endl;
        }
	}

	vGrabber.setDeviceID(devices.size() - 1);
	vGrabber.initGrabber(photoWidth, photoHeight);

	clear();
}

void Photo::clear()
{
    comp.begin();
    ofClear(0);
    comp.end();

    for (int i = 0; i < photos.size(); i++)
    {
        delete photos[i];
    }
    photos.clear();

    shootImage = false;
    cPhoto = -1;

    printf("Photo clear\n");
}

void Photo::update()
{
    vGrabber.update();
    //if (vGrabber.isFrameNew())
    {
        if (shootImage)
        {
            //printf("SHOOT: %i\n", cPhoto);
            ofTexture * texture = new ofTexture();
            texture->loadData(vGrabber.getPixels(), photoWidth, photoHeight, GL_RGB);
            texture->setAnchorPercent(0.5, 0.5);
            photos.push_back(texture);
            generateComp();
            cPhoto++;
            shootImage = false;
        }
    }
}

void Photo::shoot()
{
    if (photos.size() >= 4) return;

    shootImage = true;
}

void Photo::save(string fileName)
{
    generateComp();
    ofPixels pix;
    comp.readToPixels(pix);
    ofSaveImage(pix, fileName, OF_IMAGE_QUALITY_HIGH);
}

void Photo::generateComp()
{
    comp.begin();
    ofClear(0);
    ofPushMatrix();
    ofScale(comp.getWidth() / 1024.f, comp.getHeight() / 768.f);
    float x = 0;
    float y = 0;

    for (int i = 0; i < photos.size(); i++)
    {
        ofSetColor(255);
        ofPushMatrix();
            ofTranslate(photoPos[i].x, photoPos[i].y);
            ofRotate(photoPos[i].z);
            ofScale(photoPos[i].w, photoPos[i].w);
            photos[i]->draw(0, 0);
        ofPopMatrix();
    }
    ofPopMatrix();

    ofSetColor(255);
    fg.draw(0, 0);

    comp.end();
}
