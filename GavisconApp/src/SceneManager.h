#ifndef __SceneManager_h__
#define __SceneManager_h__

#include "ofMain.h"
#include "BaseScene.h"
#include "BaseSceneTransition.h"
#include "SceneTransitionAlpha.h"

class SceneManager
{
    public:
        SceneManager()
        {
            sceneTransition = NULL;
            currentScene = prevScene = NULL;
            transitionInterval = 1;
            transitionStarted = -1;
            setTransition(new SceneTransitionAlpha());
        };

        ~SceneManager(){};

        void setup(ofFbo::Settings s)
        {
            fbo1.allocate(s);
            fbo2.allocate(s);
            fbo1.begin();
            ofClear(0, 0);
            fbo1.end();
            fbo2.begin();
            ofClear(0, 0);
            fbo2.end();
            
            view.set(0, 0, s.width, s.height);
        };

        void add(BaseScene * scene, int id)
        {
            scene->sceneId = id;
            scene->sceneWidth = fbo1.getWidth();
            scene->sceneHeight = fbo1.getHeight();
            scenes.push_back(scene);
        };

        BaseScene * getSceneById(int id)
        {
            for (vector<BaseScene*>::iterator i = scenes.begin(); i != scenes.end(); i++)
                if ((*i)->sceneId == id) return *i;
            return NULL;
        }

        void setTransition(BaseSceneTransition * _sceneTransition, float _transitionInterval = 1)
        {
            if (sceneTransition) delete sceneTransition;
            sceneTransition = _sceneTransition;
            sceneTransition->init((int)fbo1.getWidth(), (int)fbo1.getHeight());
            transitionInterval = _transitionInterval;
        }

        void switchTo(int id)
        {
            if (getSceneById(id) == currentScene) return;

            if (!currentScene)
            {
                transitionPercent = 1;
                currentScene = getSceneById(id);
                if (currentScene)
                {
                    currentScene->setup();
                    currentScene->sceneAlive = true;
                }
                else
                {
                    ofLog(OF_LOG_ERROR, "NiSceneManager::No scene assigned to id: %i\n", id);
                    OF_EXIT_APP(0);
                }
            }
            else
            {
                if (prevScene)
                {
                    if (prevScene->sceneAlive)
                    {
                        prevScene->sceneAlive = false;
                        prevScene->destroy();
                    }
                }
                prevScene = currentScene;
                currentScene = getSceneById(id);
                if (currentScene)
                {
                    currentScene->setup();
                    currentScene->sceneAlive = true;
                }
                else
                {
                    ofLog(OF_LOG_ERROR, "NiSceneManager::No scene assigned to id: %i\n", id);
                    OF_EXIT_APP(0);
                }
                transitionPercent = 0;
                transitionStarted = ofGetElapsedTimef();
            }
        };

        void update()
        {
            transitionPercent = ofMap(ofGetElapsedTimef(), transitionStarted, transitionStarted + transitionInterval, 0, 1, true);
            if (transitionPercent == 1)
            {
                if (prevScene)
                {
                    prevScene->sceneAlive = false;
                    prevScene->destroy();
                }
                prevScene = NULL;
            }
            else if (prevScene) prevScene->update();
			if (currentScene) currentScene->update();
        };

        void draw()
        {
            if (prevScene)
            {
                fbo1.begin();
                    prevScene->draw();
                fbo1.end();
            }
            if (currentScene)
            {
                fbo2.begin();
                    currentScene->draw();
                fbo2.end();
            }
            if (transitionPercent < 1)
                sceneTransition->draw(&fbo1, &fbo2, transitionPercent);
            else
            {
                ofSetColor(255);
                fbo2.draw(0, 0);
            }
        };
    
    bool isReady()
    {
        return ofMap(ofGetElapsedTimef(), transitionStarted, transitionStarted + transitionInterval, 0, 1, true) == 1;
    }
    
    void onTouch(ofVec2f p)
    {
        if (currentScene) currentScene->onTouch(p);
    };
    
    void onDrag(ofVec2f p)
    {
        if (currentScene) currentScene->onDrag(p);
    };
    
    void onRelease(ofVec2f p)
    {
        if (currentScene) currentScene->onRelease(p);
    };
    
    void onPan(ofVec2f p)
    {
        if (currentScene) currentScene->onPan(p);
    };
    
    void onZoom(ofVec2f p, float amount)
    {
        if (currentScene) currentScene->onZoom(p, amount);
    };

    vector<BaseScene*> scenes;
    BaseScene * currentScene, * prevScene;
    ofFbo fbo1, fbo2;
    float transitionPercent;
    float transitionStarted;
    float transitionInterval;
    BaseSceneTransition * sceneTransition;
    ofRectangle view;
};

#endif

