//
//  Object.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Object_h
#define GameplayDemo_Object_h

#include "ofMain.h"
#include "Fire.h"
#include "RenderListElement.h"
#include "Parameters.h"

class Object : public RenderListElement
{
public:
    Object(ofVec3f _p, ofTexture * _texture)
    {
        p = _p;
        texture = _texture;
        alive = true;
        rot = ofRandom(0, 360);
        rotSpeed = ofRandom(1, 3);
        if (ofRandomf() < 0) rotSpeed *= -1;

        friction = 1;

        killInterval = 2;
        killStarted = -1;

    }

    void update(float dt)
    {

        float g = -ofMap(Parameters::levelPct, 0, 1, Parameters::foodGravMin, Parameters::foodGravMax);
        v += ofVec3f(0, g, 0);
        p += v;
        rot += rotSpeed;

        if (p.y < -1000)
        {
            alive = false;
        }
        if (killStarted != -1 && (ofGetElapsedTimef() - killStarted > killInterval))
        {
            alive = false;
        }
    }

    void kill()
    {
        rotSpeed = 0;
        killStarted = ofGetElapsedTimef();
    }
    
    void bounce()
    {
        v.y *= -0.6;
        rotSpeed *= -0.9;
    }
    
    void drawn()
    {
        v.y *= 0.1;
    }

    void draw()
    {
        float w = 190;
        float h = texture->getHeight() / texture->getWidth() * w;

        ofSetColor(255);
        ofPushMatrix();
        ofTranslate(p);
        ofRotate(rot, 0, 0, 1);
        if (killStarted != -1) ofSetColor(255, ofMap(ofGetElapsedTimef(), killStarted, killStarted + killInterval, 255, 0));
        texture->draw(- w / 2., - h / 2., w, h);
        ofPopMatrix();
    }

    bool alive;
    ofVec3f v;
    ofTexture * texture;
    float rot, rotSpeed;
    float killStarted, killInterval;
    float friction;
};

#endif
