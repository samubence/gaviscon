#include "ofApp.h"
#include "ofxWebServerWebSocketRoute.h"
#include "LetterBoxView.h"

#include "SceneTransitionGlass.h"
#include "SceneTransitionSlide.h"

#include "Fonts.h"
#include "Texts.h"

#include "Sfx.h"
#include "xioAllElements.h"
/*
 TODO:


 */
//--------------------------------------------------------------
void ofApp::setup()
{
    //ofSetVerticalSync(true);
    ofSetFrameRate(60);
    ofEnableAlphaBlending();
    ofSetLogLevel(OF_LOG_NOTICE);
#ifdef TARGET_WIN32
    myIP = MyIP::getLocal();
#endif

    Parameters::load();
    // LOAD TEXTS, FONTS
    Fonts::load();
    Texts::load();

    // LOAD SOUNDS
    Sfx::init();

    ofxXmlSettings xml;
    if (xml.load("lang.xml"))
    {
        Texts::lang = xml.getValue("lang:id", 0);
    }

    // START WEBSERVER
    rootDir = "DocumentRoot";
    server = new WebServer(this, 8080, rootDir);
    server->addRoute(ofxWebServerWebSocketRoute::Instance());
    server->start();

    oscReceiver.setup(12345);

    photo.setup();

    ofFbo::Settings s;
    s.width = 1024;
    s.height = 768;
    s.internalformat = GL_RGB;
    s.useDepth = true;
    //s.numSamples = ofFbo::maxSamples();

    sceneMan.setup(s);

    sceneMan.add(&sDemo, SCENE_DEMO);
    sceneMan.add(&sGame, SCENE_GAME);
    sceneMan.add(&sGameOver, SCENE_GAME_OVER);

    sDemo.init();
    sGame.init();
    ofAddListener(sGame.sceneEvent, this, &ofApp::gameEvent);
    sGameOver.init();

    debugDisplay = false;

#ifdef EDIT_MODE
    // for level editor
    cLayer = sGame.layers.begin();
#endif

    // for easy thread safe starting
    doStartDemo = false;
    doStartGame = false;

    //sGameOver.setPhoto(&photo.comp.getTextureReference());sGameOver.addScore(1, Score(250, ofGetTimestampString()));sceneMan.switchTo(SCENE_GAME_OVER);
    sceneMan.switchTo(SCENE_DEMO);
    //sceneMan.switchTo(SCENE_GAME);

    xio.setStyle(new xioDefaultStyleAlpha());
    xioWindow * w = xio.createWindow("settings", 0, 0, 400, 1000);
    w->addElement(this, new xio_PushButton("IP: " + myIP));
    w->addGroup("photo");
    w->addElement(this, new xio_ImageViewer("", &photo.vGrabber.getTextureReference()));
    w->addGroup("calibration");
    w->addElement(this, new xio_PushButton("reset"), XIO_RESET);
    w->addElement(this, new xio_Slider("LEFT yaw", -45, 45, &(sGame.gun[0]->baseShift.x)));
    w->addElement(this, new xio_Slider("LEFT pitch", -45, 45, &(sGame.gun[0]->baseShift.y)));
    w->addElement(this, new xio_Slider("RIGHT yaw", -45, 45, &(sGame.gun[1]->baseShift.x)));
    w->addElement(this, new xio_Slider("RIGHT pitch", -45, 45, &(sGame.gun[1]->baseShift.y)));
    w->addElement(this, new xio_PushButton("save"), XIO_SAVE);
    xio.load("calibration.xml");

    ofHideCursor();
}

//--------------------------------------------------------------
void ofApp::update()
{

    if (doStartDemo)
    {
        sceneMan.setTransition(new SceneTransitionSlide());
        sceneMan.switchTo(SCENE_DEMO);
        doStartDemo = false;
    }

    if (doStartGame)
    {
        /*
        sceneMan.setTransition(new SceneTransitionSlide());
        sceneMan.switchTo(SCENE_GAME);
        */
        startGame();
        doStartGame = false;
    }

    while (oscReceiver.hasWaitingMessages())
    {
        ofxOscMessage m;
        oscReceiver.getNextMessage(&m);

        if (m.getAddress() == "/e0")
        {
            ofVec3f euler = ofVec3f(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2));
            sGame.gun[0]->setEuler(euler);
        }

        if (m.getAddress() == "/e1")
        {
            ofVec3f euler = ofVec3f(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2));
            sGame.gun[1]->setEuler(euler);
        }

    }

    sceneMan.update();

    Sfx::update();

    photo.update();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(0);

    LetterBoxView::begin(sceneMan.view);
    ofSetColor(255);
    sceneMan.draw();
    LetterBoxView::end();

    if (debugDisplay)
    {
        xio.draw();
    }
}

void ofApp::welcomeEvent(int &e)
{
    startGame();
}

void ofApp::gameEvent(int &e)
{
    if (e == SceneGame::EVENT_TAKE_PHOTO)
    {
        photo.shoot();
    }
    else
    {
        Sfx::fadeGameLoop(false);
        Sfx::fadeTimeUpLoop(false);

        photo.save(rootDir + "/" + fileToSend + ".jpg");

        sGameOver.addScore(sGame.level, Score((int)sGame.tScore, sGame.getPlayers()));
        sGameOver.setPhoto(&photo.comp.getTextureReference());

        sceneMan.setTransition(new SceneTransitionGlass(), 3);
        sceneMan.switchTo(SCENE_GAME_OVER);
        //startGame();
    }
}
/*
void ofApp::introEvent(int &e)
{
    startGame();
}
*/
void ofApp::startGame()
{
    //printf("START GAME\n");
    if (sceneMan.currentScene != &sGame)
    {
        fileToSend = "Gaviscon_" + ofToString(ofGetYear()) + "_" + ofToString(ofGetMonth()) + "_" + ofToString(ofGetDay()) + "_" + ofToString(ofGetHours()) + "_" + ofToString(ofGetMinutes()) + "_" + ofToString(ofGetSeconds());
        sceneMan.setTransition(new SceneTransitionAlpha(), 1);
        sceneMan.switchTo(SCENE_GAME);
        photo.clear();
    }
}

void ofApp::handleRequest(HTTPServerRequest& request, HTTPServerResponse& response)
{
    URI uri(request.getURI());
    string message = uri.getPath();
    message.erase(0, 1);
    vector<string> parameters = ofSplitString(message, ",");
    //printf("MESSAGE: %s  :: p: %i\n", message.c_str(), parameters.size());
    string responseStr = ":(";

    if (parameters.size() == 1)
    {
        if (parameters[0] == "getCurrentImage")
        {
            if (sceneMan.currentScene == &sGameOver)
                responseStr = fileToSend + ".jpg";
            else
                responseStr = "";

        }
        else if (parameters[0] == "getCurrentUsers")
        {
            responseStr = sGame.getPlayers();
        }
        else if (parameters[0] == "home")
        {
            doStartDemo = true;
            responseStr = "ok";
        }
        else
        {
            //printf("sending file: %s\n", (rootDir + "/" + parameters[0]).c_str());
            ofFile file(rootDir + "/" + parameters[0]);
            try
            {
                MediaType mediaType = ofxWebServerGetMimeType(file.getExtension());
                response.sendFile(file.getAbsolutePath(), mediaType.toString());
                return;
            }
            catch (const std::exception& ex)
            {
                ofLogError("ofxWebServerDefaultRouteHandler::handleRequest") << "Unknown server error: " << ex.what();
            }
        }
    }
    else if (parameters.size() == 2)
    {
        if (parameters[0] == "lang")
        {
            Texts::lang = ofToInt(parameters[1]);
            responseStr = "ok";

            ofxXmlSettings xml;
            xml.addValue("lang::id", Texts::lang);
            xml.save("lang.xml");
        }
        else if (parameters[0] == "start")
        {
            vector<string> userNames;
            userNames.push_back(parameters[1]);
            sGame.setPlayers(userNames);
            doStartGame = true;
            responseStr = "ok";
        }
    }
    else if (parameters.size() == 3)
    {
        if (parameters[0] == "start")
        {
            vector<string> userNames;
            userNames.push_back(parameters[1]);
            userNames.push_back(parameters[2]);
            sGame.setPlayers(userNames);
            doStartGame = true;
            responseStr = "ok";
        }
    }
    else if (parameters.size() == 4)
    {
        /*
        if (parameters[0] == "e0")
        {
            ofVec3f euler(ofToFloat(parameters[1]), ofToFloat(parameters[2]), ofToFloat(parameters[3]));
            //printf("%f %f %f\n", euler.x, euler.y, euler.z);
            if (sceneMan.currentScene == &sGame)
            {
                float xsc = 2;
                float ysc = 2;
                sGame.gun[0]->setGlobalOrientation(ofQuaternion());
                sGame.gun[0]->rotate((-euler.x * xsc), 0, 1, 0);
                sGame.gun[0]->rotate((-euler.y * ysc), 1, 0, 0);
            }
            responseStr = "ok";
        }

        if (parameters[0] == "e1")
        {
            ofVec3f euler(ofToFloat(parameters[1]), ofToFloat(parameters[2]), ofToFloat(parameters[3]));
            //printf("%f %f %f\n", euler.x, euler.y, euler.z);
            if (sceneMan.currentScene == &sGame)
            {
                sGame.gun[1]->setGlobalOrientation(ofQuaternion());
                sGame.gun[1]->rotate((euler.x), 0, 1, 0);
                sGame.gun[1]->rotate((euler.z), 1, 0, 0);
            }
            responseStr = "ok";
        }
        */
        if (parameters[0] == "store")
        {
            storeUserData(parameters[1], parameters[2], parameters[3]);
            responseStr = "ok";
        }
    }

    response.sendBuffer(responseStr.c_str(), responseStr.size());
}

void ofApp::xioEvent(xioEventArgs &e)
{
    if (e.id == XIO_SAVE)
    {
        xio.save("calibration.xml");
    }
    if (e.id == XIO_RESET)
    {
        sGame.gun[0]->reset();
        sGame.gun[1]->reset();
    }
}

void ofApp::storeUserData(string email, string name, string date)
{
    ofBuffer buffer = ofBufferFromFile("userdata.csv");
    buffer.append(email + "," + name + "," + date + "\n");
    ofBufferToFile("userdata.csv", buffer);
    printf("User data saved: %s %s %s\n", email.c_str(), name.c_str(), date.c_str());
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == 'f') ofToggleFullscreen();
    if (key == 'd')
    {
        debugDisplay ^= true;
        debugDisplay ? ofShowCursor() : ofHideCursor();
    }
    if (key == ' ') doStartGame = true;
#ifdef EDIT_MODE
    if (key == 's')
    {
        ofxXmlSettings xml;
        sGame.lIntroBg.save(&xml, "layerIntroBg");
        sGame.lIntro.save(&xml, "layerIntro");
        sGame.lBg.save(&xml, "layerBg");
        sGame.lLevel.save(&xml, "layerLevel");
        sGame.lFg.save(&xml, "layerFg");vector<Object*> objs;

        xml.save("settings.xml");
    }

    ofVec3f v;
    float scale = 1;
    if (key == OF_KEY_LEFT) v.x = -10;
    if (key == OF_KEY_RIGHT) v.x = 10;
    if (key == OF_KEY_UP) v.y = 10;
    if (key == OF_KEY_DOWN) v.y = -10;
    if (key == '.') v.z = 10;
    if (key == ',') v.z = -10;

    if (key == OF_KEY_PAGE_UP) scale = 1.01;
    if (key == OF_KEY_PAGE_DOWN) scale = 0.99;

    if (key == 'n')
    {
        cLayer++;
        if (cLayer >= sGame.layers.end()) cLayer = sGame.layers.begin();
    }

    (*cLayer)->p += v;
    (*cLayer)->scale *= scale;
#endif
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
