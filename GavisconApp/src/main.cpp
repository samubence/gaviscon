#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	ofSetupOpenGL(800, 600, OF_WINDOW);
	//ofSetupOpenGL(1024, 768, OF_WINDOW);
    ofRunApp(new ofApp());

}
