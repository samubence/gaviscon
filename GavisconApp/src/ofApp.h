#pragma once

#include "ofMain.h"
#include "WebServer.h"
#include "ofxOscReceiver.h"

#include "SceneManager.h"
#include "SceneDemo.h"
#include "SceneGame.h"
#include "SceneGameOver.h"

#include "XIO.h"


//#define EDIT_MODE

#include "Photo.h"
#ifdef TARGET_WIN32
#include "MyIP.h"
#endif


class ofApp : public ofBaseApp, public WebServerListener, xioListener{

public:
    enum
    {
        SCENE_DEMO,
        SCENE_GAME,
        SCENE_GAME_OVER
    };
    enum
    {
        XIO_RESET,
        XIO_SAVE
    };
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    void xioEvent(xioEventArgs & e);

    void welcomeEvent(int & e);
    void gameEvent(int & e);
    //void introEvent(int & e);
    void startGame();

    void storeUserData(string email, string name, string date);

    XIO xio;

    SceneManager sceneMan;
    SceneDemo sDemo;
    SceneGame sGame;
    SceneGameOver sGameOver;

    ofxOscReceiver oscReceiver;

    Photo photo;

    void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response);
    WebServer * server;
    string rootDir;

    string fileToSend;
    bool doStartGame, doStartDemo;

    string myIP;
    bool debugDisplay;

#ifdef EDIT_MODE
    // editor
    vector<Layer*>::iterator cLayer;
#endif

};
