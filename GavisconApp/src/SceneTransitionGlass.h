//
//  SceneTransitionGlass.h
//  MadachRetro
//
//  Created by bence samu on 28/05/14.
//
//

#ifndef _SceneTransitionGlass_h
#define _SceneTransitionGlass_h

#include "BaseSceneTransition.h"
#include "ofxTriangle.h"

class TriangleTexture
{
public:
    ofPoint p1, p2, p3;
    ofPoint t1, t2, t3;
    ofVec3f pos, vel, rot, spin;
    float g;
    TriangleTexture(ofPoint _p1, ofPoint _p2, ofPoint _p3)
    {
        pos = (_p1 + _p2 + _p3) / 3.;
        p1 = _p1 - pos;
        p2 = _p2 - pos;
        p3 = _p3 - pos;
        t1 = _p1;
        t2 = _p2;
        t3 = _p3;
        spin = ofVec3f(ofRandomf(), ofRandomf(), ofRandomf()) * 10;
        g = ofRandom(0.1, 0.5);
    };
    
    void draw(ofTexture * texture, float p)
    {
        vel += ofVec3f(0, g, 0);
        pos += vel;
        rot += spin * p;
        
        ofPushMatrix();
        ofTranslate(pos);
        ofRotate(rot.x, 1, 0, 0);
        ofRotate(rot.y, 0, 1, 0);
        ofRotate(rot.z, 0, 0, 1);
        
        ofDisableNormalizedTexCoords();
        texture->bind();
        glBegin(GL_TRIANGLES);
        
        glTexCoord2f(t1.x, t1.y);
        glVertex2f(p1.x, p1.y);
        
        glTexCoord2f(t2.x, t2.y);
        glVertex2f(p2.x, p2.y);
        
        glTexCoord2f(t3.x, t3.y);
        glVertex2f(p3.x, p3.y);
        
        glEnd();
        texture->unbind();
        
        ofPopMatrix();
    };
};
class SceneTransitionGlass : public BaseSceneTransition
{
public:
    void init(int width, int height)
    {
        triangle.clear();
        for (int i = 0; i < tris.size(); i++)
        {
            delete tris[i];
        }
        tris.clear();
        vector<ofPoint> pts;
        pts.push_back(ofPoint(0, 0));
        pts.push_back(ofPoint(width, 0));
        pts.push_back(ofPoint(width, height));
        pts.push_back(ofPoint(0, height));
        float border = 300;
        for (int i = 0; i < 200; i++)
        {
            pts.push_back(ofPoint(ofRandom(-border, width + border * 2), ofRandom(-border, height + border * 2)));
        }
        triangle.triangulate(pts, false, pts.size());
        for (int i = 0; i < triangle.nTriangles; i++)
        {
            ofxTriangleData * td = &triangle.triangles[i];
            TriangleTexture * t = new TriangleTexture(td->a, td->b, td->c);
            tris.push_back(t);
        }
    };
    
    void draw(ofFbo * fbo1, ofFbo * fbo2, float p)
    {
        ofSetColor(255);
        fbo2->draw(0, 0);
        for (int i = 0; i < tris.size(); i++)
        {
            tris[i]->draw(&fbo1->getTextureReference(), p);
        }
    };
    
    ofxTriangle triangle;
    vector<TriangleTexture*> tris;
};

#endif
