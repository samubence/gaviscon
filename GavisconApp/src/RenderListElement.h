#ifndef RENDERLISTELEMENT_H_INCLUDED
#define RENDERLISTELEMENT_H_INCLUDED

#include "ofMain.h"

class RenderListElement
{
    public:

        RenderListElement(){};
        ~RenderListElement(){};

        virtual void draw(){};

    ofVec3f p;
};


#endif // RENDERLISTELEMENT_H_INCLUDED
