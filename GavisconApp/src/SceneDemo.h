//
//  SceneDemo.h
//  GavisconApp
//
//  Created by bence samu on 03/09/14.
//
//

#ifndef __GavisconApp__SceneDemo__
#define __GavisconApp__SceneDemo__

#include "ofMain.h"
#include "BaseScene.h"

class SceneDemo : public BaseScene
{
public:
    void init();
    void setup();
    void destroy();
    void update();
    void draw();
    void drawVideo();
    void drawScore();
    
    ofTexture bg, fg, claimFg;
    ofRectangle scoreView;
    vector< pair<int, string> > scores;
    
    ofVideoPlayer vPlayer;

};

#endif /* defined(__GavisconApp__SceneDemo__) */
