//
//  Texts.cpp
//  GavisconApp
//
//  Created by bence samu on 12/08/14.
//
//

#include "Texts.h"
#include "ofxXmlSettings.h"

vector< vector<string> > Texts::texts;
int Texts::lang;

void Texts::load()
{
    ofxXmlSettings xml;
    xml.load("texts.xml");
    xml.pushTag("texts");
    int n = xml.getNumTags("lang");
    for (int i = 0; i < n; i++)
    {
        vector<string> elements;
        
        xml.pushTag("lang", i);
        int nTexts = xml.getNumTags("text");
        for (int j = 0; j < nTexts; j++)
        {
            xml.pushTag("text", j);
            string txt = xml.getValue("t", "");
                elements.push_back(txt);
                //printf("%i-%i: %s\n", i, j, txt.c_str());
            xml.popTag();
        }
        xml.popTag();
        
        texts.push_back(elements);
    }
    
    xml.popTag();

    lang = HUN;
}

string Texts::getText(int n)
{
    if (n >= 0 && n < texts[0].size())
    {
        return texts[lang][n];
    }
    else return "";
}