//
//  SceneGameOver.h
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#ifndef __GavisconApp__SceneGameOver__
#define __GavisconApp__SceneGameOver__

#include "ofMain.h"
#include "BaseScene.h"
#include "Acid.h"

class Score
{
public:
    Score(int _value, string _name)
    {
        name = _name;
        value = _value;
    };
    
    string name;
    int value;
};

class SceneGameOver : public BaseScene
{
public:
    void init();
    void setup();
    void destroy();
    void update();
    void draw();
    void drawPhoto(float a);
    void drawScore(float a);
    void drawClaim(float a);

    void setPhoto(ofTexture * texture);
    void addScore(int level, Score score);
    void save();
    void clearScore();
    void loadScore();

    ofTexture bg, fg, claimFg;
    ofRectangle scoreView;
    vector< Score > scores;
    ofTexture * photoTex;
    Acid acid;
    int cScore;

    float started, delayInterval, fadeInterval;
    int level;
    bool isPlaying;

    static bool compScore( Score v1, Score v2);
};

#endif /* defined(__GavisconApp__SceneGameOver__) */
