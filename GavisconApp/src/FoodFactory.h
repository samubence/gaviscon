#ifndef __FoodFactory_h__
#define __FoodFactory_h__

#include "ofMain.h"
#include "Object.h"

class FoodFactory
{
    public:
        FoodFactory();
        ~FoodFactory();

        void setup();
        void update();
        void reset();

        vector<Object*> objs;
        vector<ofTexture*> foodTexs;
        float started, interval;
        float iMin, iMax;
};

#endif

