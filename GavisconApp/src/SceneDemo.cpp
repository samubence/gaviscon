//
//  SceneWelcome.cpp
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#include "SceneDemo.h"
#include "LetterBoxView.h"
#include "ofxXmlSettings.h"
#include "Fonts.h"
#include "Texts.h"
#include "Sfx.h"

void SceneDemo::init()
{
    ofLoadImage(bg, "gfx/gameOver/bg.png");
    ofLoadImage(fg, "gfx/welcome/fg.png");
    vPlayer.loadMovie("gfx/demo" + ofToString(Texts::lang) + ".mov");
    vPlayer.setLoopState(OF_LOOP_NORMAL);
}

void SceneDemo::setup()
{
    //printf("SceneWelcome::setup()\n");
    
    scores.clear();
    ofxXmlSettings xml;
    if (xml.load("score.xml"))
    {
        xml.pushTag("scores");
        int n = xml.getNumTags("element");
        for (int i = 0; i < n; i++)
        {
            xml.pushTag("element", i);
            scores.push_back(make_pair(xml.getValue("score", 0), xml.getValue("name", "")));
            xml.popTag();
        }
        xml.popTag();
    }
    
    vPlayer.play();
    Sfx::fadeWelcomeLoop(true);
}

void SceneDemo::destroy()
{
    vPlayer.stop();
    vPlayer.setPosition(0);
    Sfx::fadeWelcomeLoop(false);
}

void SceneDemo::update()
{
    vPlayer.update();
}

void SceneDemo::draw()
{
    ofClear(0);
    
    ofSetColor(255);
    bg.draw(0, 0);
    
    drawVideo();
    
    ofSetColor(255);
    fg.draw(0, 0);
    
    drawScore();
}

void SceneDemo::drawVideo()
{
    ofRectangle photoView(ofGetMouseX(), ofGetMouseY(), 500, 700);
    //printf("%f, %f, %f, %f\n", photoView.x, photoView.y, photoView.width, photoView.height);
    photoView.set(500, 32.000000, 500.000000, 700.000000);
    ofRectangle texView(0, 0, vPlayer.getWidth(), vPlayer.getHeight());
    LetterBoxView::begin(texView, photoView);
    ofSetColor(255, 255);
    vPlayer.draw(0, 0);
    ofNoFill();
    LetterBoxView::end();
}

void SceneDemo::drawScore()
{
    scoreView.set(25, 250, 450, 310);//ofGetMouseY());//, 450.000000, 370.000000);
    //printf("%f, %f, %f, %f\n", scoreView.x, scoreView.y, scoreView.width, scoreView.height);ofSetColor(0);ofNoFill();ofRect(scoreView);
    
    float scoreSize = 35;
    
    ofSetColor(247, 207, 22);
    Fonts::draw(Texts::getText(Texts::HIGH_SCORE), ofVec2f(scoreView.x + scoreView.width / 2., scoreView.y - 20), scoreSize * 1.2, Fonts::ALIGN_CENTER);
    
    float y = scoreView.y + scoreSize;
    
    for (int i = 0; i < scores.size(); i++)
    {
        if (y >= scoreView.y + scoreSize && y < scoreView.y + scoreView.height)
        {
            ofSetColor(0);
            Fonts::draw(ofToString(i + 1) + ". " + scores[i].second, ofVec2f(scoreView.x + scoreSize, y), scoreSize, Fonts::ALIGN_LEFT);
            string score = ofToString(scores[i].first);
            if (score.length() >= 4) score.insert(1, ".");
            Fonts::draw(score, ofVec2f(scoreView.x + scoreView.width - scoreSize, y), scoreSize, Fonts::ALIGN_RIGHT);
        }
        y += scoreSize;
        
    }
    
}
