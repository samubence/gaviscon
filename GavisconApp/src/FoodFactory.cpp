#include "FoodFactory.h"
#include "Parameters.h"
#include "ofxXmlSettings.h"

FoodFactory::FoodFactory()
{

}

FoodFactory::~FoodFactory()
{

}

void FoodFactory::setup()
{
    ofxXmlSettings xml;
    if (xml.load("food.xml"))
    {
        xml.pushTag("food");
        int n = xml.getNumTags("img");
        for (int i = 0; i < n; i++)
        {
            xml.pushTag("img", i);
            ofTexture * foodTex = new ofTexture();
            ofLoadImage(*foodTex, xml.getValue("fileName", ""));
            foodTexs.push_back(foodTex);
            xml.popTag();
        }
        xml.popTag();
    }
    
    reset();
    
    interval = ofRandom(iMin, iMax);
    started = ofGetElapsedTimef() - interval;
}

void FoodFactory::update()
{
    if (ofGetElapsedTimef() - started > interval)
    {
        interval = ofRandom(iMin, iMax);
        started = ofGetElapsedTimef();

        float space = 600;
        ofVec3f p(ofRandom(-space, space), 600, ofRandom(20, 30));
        int n = (int)ofRandom(0, foodTexs.size());
        Object * o = new Object(p, foodTexs[n]);
        objs.push_back(o);
    }
}

void FoodFactory::reset()
{
    iMin = ofMap(Parameters::levelPct, 0, 1, Parameters::foodIntervalMinEasy, Parameters::foodIntervalMinHard);
    iMax = ofMap(Parameters::levelPct, 0, 1, 4, 2);
    
}
