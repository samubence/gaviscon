//
//  Acid.cpp
//  GavisconApp
//
//  Created by bence samu on 16/08/14.
//
//

#include "Acid.h"


ofTexture Acid::texture;
/*
int Acid::numVerts;
ofVbo Acid::vbo;
*/
void Acid::init()
{
    ofLoadImage(texture, "gfx/game/bubble.png");
    texture.setAnchorPercent(.5, .5);
    /*
    float w = 1;
    float h = 1;
    
    ofMesh msh;
    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));
    
    msh.addTexCoord(ofVec2f(w, 0));
    msh.addVertex(ofVec2f(w/2., -h/2.));
    
    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));
    
    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));
    
    msh.addTexCoord(ofVec2f(0, h));
    msh.addVertex(ofVec2f(-w/2., h/2));
    
    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));
    
    vbo.setMesh(msh, GL_STATIC_DRAW);
    numVerts = msh.getNumVertices();    
     */
}

AcidParticle::AcidParticle(ofVec2f _p, float _r, ofRectangle _view, int _type)
{
    p = _p;
    pr = r = _r;
    view = _view;
    friction = ofRandom(0.7, 0.8);
    started = ofGetElapsedTimef();
    type = _type;
    if (type == ACID_DOWN)
    {
        interval = ofRandom(4, 6);
    }
    else if (type == ACID_UP)
    {
        interval = ofRandom(0.2, 1);
    }
    else if (type == ACID_BUBBLE)
    {
        interval = ofRandom(6, 10);
    }
    alive = true;
}

void AcidParticle::borders()
{
    if (p.y < view.y + r)
    {
        v.y *= -friction * 0.5;
        p.y = view.y + r;
    }
    if (p.x < view.x + r)
    {
        p.x = view.x + r;
    }
    if (p.x > view.x + view.width - r)
    {
        p.x = view.x + view.width - r;
    }
}

void AcidParticle::update()
{
    if (type == ACID_UP || type == ACID_BUBBLE)
    {
        float freq = 0.1;
        float sc = 0.01;
        ofVec2f f((ofNoise(p.x * sc, p.y * sc, ofGetElapsedTimef() * freq) * 2 - 1) * 0.2, -0.53);
        v += f;
    }
    else if (type == ACID_DOWN)
    {
        float freq = 0.1;
        float sc = 0.01;
        float nx = ofNoise(p.x * sc, p.y * sc, ofGetElapsedTimef() * freq, 123.456);
        float ny = ofNoise(p.x * sc, p.y * sc, ofGetElapsedTimef() * freq, 987.654);
        ofVec2f f(ofMap(nx, 0, 1, -0.5, 0.5), ofMap(ny, 0, 1, -0.2, -0.6));
        v += f;
    }
    
    v *= friction;
    
    p += v;
    
    
    float life = ofMap(ofGetElapsedTimef(), started, started + interval, 0, 1, true);
    if (life == 1) alive = false;
    
    r = ofMap(life, 0.7, 1, pr, 0, true);
    if (life < 0.05) r = ofMap(life, 0, 0.05, 0, pr);
}

void AcidParticle::draw()
{
    //if (type == ACID_DOWN || type == ACID_UP)
    {
        ofSetColor(255);//, 100, 100, 200);
        Acid::texture.draw(p, r * 2, r * 2);
    }
    /*
    else if (type == ACID_BUBBLE)
    {
        ofSetColor(255, 255, 0, 30);
        ofFill();
        ofCircle(p, r);
        
        ofSetColor(255, 255, 180, 230);
        ofNoFill();
        ofCircle(p, r);
    }
     */
    /*
    if (up)
        ofSetColor(255, 255, 0, 30);
    else
        ofSetColor(255, 255, 0, 30);
    
    ofFill();
    ofCircle(p, r);
    
    if (up)
        ofSetColor(255, 255, 0, 90);
    else
        ofSetColor(255, 255, 255, 190);
    
    ofNoFill();
    ofCircle(p, r);
     */
    /*
    if (up)
        ofSetColor(255, 255, 0, 90);
    else
        ofSetColor(255, 255, 180, 230);
    ofSetColor(255);
     */
    
    /*
    ofPushMatrix();
    ofTranslate(p);
    ofScale(r * 2, r * 2);
    
    Acid::vbo.bind();
    Acid::texture.bind();
    glDrawArrays(GL_TRIANGLES, 0, Acid::numVerts);
    Acid::texture.unbind();
    Acid::vbo.unbind();
    
    ofPopMatrix();
     */
}

void AcidParticle::collide(AcidParticle * other)
{
    float force = 0.1;
    float dx = other->p.x - p.x;
    float dy = other->p.y - p.y;
    float d = sqrt(dx * dx + dy * dy);
    float td = r + other->r;
    if (d < td)
    {
        dx /= d;
        dy /= d;
        v.x -= dx * (td - d) * force;
        v.y -= dy * (td - d) * force;
    }
}



/////////////////////////////////////////

void Acid::setup(ofRectangle _view, int _type)
{
    view = _view;
    type = _type;
    
    if (type == ACID_DOWN)
    {
        for (int i = 0; i < 250; i++)
        {
            add(ofVec2f(ofRandom(view.x, view.x + view.width), ofRandom(view.y, view.y + view.height)));
        }
    }
    
    iteration = 2;
}

void Acid::clear()
{
    for (int i = 0; i < particles.size(); i++)
    {
        particles[i]->alive = false;
    }
}

void Acid::add(ofVec2f pt)
{
    if (pt == ofVec2f()) pt = ofVec2f(ofRandom(view.x, view.x + view.width), view.y + view.height);

    AcidParticle * p = new AcidParticle(pt, ofRandom(20, 35), view, type);
    particles.push_back(p);
}

void Acid::update()
{
    for (int it = 0; it < iteration; it++)
    {
        for (vector<AcidParticle*>::iterator i = particles.begin(); i != particles.end(); )
        {
            if ((*i)->alive)
            {
                if (type == ACID_DOWN || type == ACID_BUBBLE)
                {
                    for (int j = 0; j < particles.size(); j++)
                    {
                        (*i)->borders();
                        if (particles[j] != *i)
                        {
                            (*i)->collide(particles[j]);
                        }
                    }
                }
                (*i)->update();
                
                i++;
            }
            else
            {
                delete *i;
                i = particles.erase(i);
            }
        }
    }
}

void Acid::draw()
{
    for (int i = 0; i < particles.size(); i++)
    {
        particles[i]->draw();
    }
}