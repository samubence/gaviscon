//
//  Acid.h
//  GavisconApp
//
//  Created by bence samu on 16/08/14.
//
//

#ifndef __GavisconApp__Acid__
#define __GavisconApp__Acid__

#include "ofMain.h"

enum
{
    ACID_DOWN,
    ACID_UP,
    ACID_BUBBLE
};

class AcidParticle
{
public:
    AcidParticle(ofVec2f p, float r, ofRectangle _view, int _type);
    void update();
    void draw();
    void collide(AcidParticle * other);
    void borders();
    
    ofVec2f p, v;
    float friction;
    float r, pr;
    ofRectangle view;
    float started, interval;
    bool alive;
    int type;
};

class Acid
{
public:
    static void init();
    void setup(ofRectangle _view, int _type);
    void clear();
    void update();
    void draw();
    void add(ofVec2f p = ofVec2f());
    
    ofRectangle view;
    vector<AcidParticle*> particles;
    int type;
    int iteration;
    
    static ofTexture texture;
/*
    static ofVbo vbo;
    static int numVerts;
 */
};

#endif /* defined(__GavisconApp__Acid__) */
