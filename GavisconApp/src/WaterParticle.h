//
//  WaterParticle.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_WaterParticle_h
#define GameplayDemo_WaterParticle_h

#include "ofMain.h"
#include "RenderListElement.h"

/*
 TASKS:
    3D particle
    physics
    collision when hitting screen plane
*/
class WaterParticle : public RenderListElement
{
public:
    WaterParticle(ofVec3f _p, ofVec3f _v);
    static void init();
    void update(float dt);
    void draw();
    void collide();
    
    bool alive;
    ofVec3f v;
    float friction;
    float started, interval;
    
    static ofVbo vbo;
    static ofTexture texture;
    static int numVerts;
};

#endif
