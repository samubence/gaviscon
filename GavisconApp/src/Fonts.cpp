//
//  Fonts.cpp
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#include "Fonts.h"

ofxFontStash Fonts::font;

void Fonts::load()
{
    font.setup("fonts/segoeuil.ttf");
}

void Fonts::draw(string text, ofVec2f p, float size, int align)
{
    ofRectangle bbox = font.getBBox(text, size, 0, 0);
    float x = p.x;
    float y = p.y;
    if (align == ALIGN_CENTER)
    {
        x = p.x - bbox.width / 2.;
    }
    else if (align == ALIGN_RIGHT)
    {
        x = p.x - bbox.width;
    }
    
    font.draw(text, size, x, y);
}

ofRectangle Fonts::getBBox(string text, ofVec2f p, float size, int align)
{
    ofRectangle bbox = font.getBBox(text, size, 0, 0);
    float x = p.x;
    float y = p.y;
    if (align == ALIGN_CENTER)
    {
        x = p.x - bbox.width / 2.;
    }
    else if (align == ALIGN_RIGHT)
    {
        x = p.x - bbox.width;
    }
    
    return font.getBBox(text, size, x, y);
}