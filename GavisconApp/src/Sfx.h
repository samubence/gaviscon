//
//  Sfx.h
//  GavisconApp
//
//  Created by bence samu on 21/08/14.
//
//

#ifndef __GavisconApp__Sfx__
#define __GavisconApp__Sfx__

#include "ofMain.h"

#define NUM_SAMPLES

class Sfx
{
public:
    static void init();
    static void playGood();
    static void playBad();
    static void playNextLevel();
    static void playStartNew();
    static void playGameOverWin();
    static void playGameOverTimeout();
    static void fadeWelcomeLoop(bool fadeIn);
    static void fadeGameLoop(bool fadeIn);
    static void fadeGameOverLoop(bool fadeIn);
    static void fadeTimeUpLoop(bool fadeIn);
    static void update();

    static ofSoundPlayer sBad;
    static ofSoundPlayer sGood;
    static ofSoundPlayer sNextLevel;
    static ofSoundPlayer sStartNew;
    static ofSoundPlayer sGameOverWin;
    static ofSoundPlayer sGameOverTimeout;
    static ofSoundPlayer sWelcomeLoop;
    static ofSoundPlayer sGameLoop;
    static ofSoundPlayer sGameOverLoop;
    static ofSoundPlayer sTimeUpLoop;

    static float welcomeLoopVol, tWelcomeLoopVol, gameLoopVol, gameOverLoopVol, tGameLoopVol, tGameOverLoopVol, timeUpLoopVol, tTimeUpLoopVol;

    static float gameMaxVol;
};

#endif /* defined(__GavisconApp__Sfx__) */
