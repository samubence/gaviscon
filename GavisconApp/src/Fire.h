//
//  Fire.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_Fire_h
#define GameplayDemo_Fire_h

#include "ofMain.h"
#include "FireParticle.h"
#include "Fire.h"

/*
 TASKS:
    generated on Object collision
    emits fire particles
    collide with water
*/

class Fire
{
public:
    //static void init();
    Fire(ofVec3f _p);
    void update(float dt);
    void draw();
    
    ofVec3f p;
    ofEvent<ofVec3f> fireEvents;
    bool alive;
    int hitCount;
    int hitsMin, hitsMax;
    int hitsToDie;
    float hitArea, hitAreaMin, hitAreaMax;
    float started, interval;
    
    //static vector<ofTexture*> seq;
    //float cFrame;
};

#endif
