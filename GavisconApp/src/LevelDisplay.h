//
//  LevelDisplay.h
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#ifndef GavisconApp_LevelDisplay_h
#define GavisconApp_LevelDisplay_h

#include "ofMain.h"
#include "Fonts.h"
#include "Parameters.h"

class LevelDisplay
{
public:
    LevelDisplay()
    {
        started = -1;
        ofLoadImage(bg, "gfx/game/circle.png");

    }
    void start(string _txt = "")
    {
        txt = _txt;
        started = ofGetElapsedTimef();
        eventFired = false;
    }

    void update()
    {
        if (started != -1)
        {
            pct = ofMap(ofGetElapsedTimef(), started, started + Parameters::nextLevelInterval, 0, 1, true);

            if (pct == 1) started = -1;     // stop animation

            if (txt == "")                  // if there is no text to display, simple fade out
            {
                if (pct > 0.5 && !eventFired)
                {
                    eventFired = true;
                    bool e = true;
                    ofNotifyEvent(fadedEvent, e);
                }
                fade = ofMap(pct, 0, 0.5, 0, 1, true);
            }
            else
            {
                if (pct == 1)
                {
                    started = -1;
                }
                else
                {
                    fade = 1;
                    if (pct < 0.5)
                    {
                        fade = ofMap(pct, 0.3, 0.5, 0, 1, true);
                    }
                    else if (pct > 0.8)
                    {
                        if (!eventFired)
                        {
                            eventFired = true;
                            bool e = true;
                            ofNotifyEvent(fadedEvent, e);
                        }
                        fade = ofMap(pct, 0.8, 1, 1, 0, true);
                    }
                }
            }
        }
        else pct = 0;
    }

    void draw(ofRectangle view)
    {
        ofVec2f center = view.getCenter();

        if (started != -1)
        {
            ofFill();
            ofSetColor(20, 139, 168, 255 * fade);
            ofRect(view);

            if (txt != "")
            {
                if (pct >= 0.5 && pct < 0.8)
                {
                    float p = ofMap(pct, 0.5, 0.8, 0, 1);
                    float size = 230;
                    float scale = (sin(p * PI) + 0.1);
                    ofPushMatrix();
                    ofTranslate(center);
                    ofScale(scale, scale);
                    ofSetColor(255);
                    bg.draw(-size / 2., -size / 2., size, size);
                    ofSetColor(255);
                    Fonts::draw(txt, ofVec2f(0, 10), 50);
                    ofPopMatrix();
                }
            }
        }
    }

    string txt;
    float started;
    ofTexture bg;
    float pct;
    float fade;
    ofEvent<bool> fadedEvent;
    bool eventFired;
};

#endif
