//
//  WaterParticle.cpp
//  GavisconApp
//
//  Created by bence samu on 06/08/14.
//
//

#include "WaterParticle.h"

ofVbo WaterParticle::vbo;
ofTexture WaterParticle::texture;
int WaterParticle::numVerts;

void WaterParticle::init()
{
    ofLoadImage(texture, "gfx/game/water.png");
    
    float w = texture.getWidth();
    float h = texture.getHeight();
    
    ofMesh msh;
    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));
    
    msh.addTexCoord(ofVec2f(w, 0));
    msh.addVertex(ofVec2f(w/2., -h/2.));
    
    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));
    
    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));
    
    msh.addTexCoord(ofVec2f(0, h));
    msh.addVertex(ofVec2f(-w/2., h/2));
    
    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));
    
    vbo.setMesh(msh, GL_STATIC_DRAW);
    numVerts = msh.getNumVertices();
}

WaterParticle::WaterParticle(ofVec3f _p, ofVec3f _v)
{
    p = _p;
    v = _v;
    alive = true;
    friction = 1;//0.99998;
    started = ofGetElapsedTimef();
    interval = 2;
}

void WaterParticle::update(float dt)
{
    float g = -0.48;
    
    v += ofVec3f(0, g, 0);
    v *= friction;
    p += v;
    
    if (p.y < -1000 || ofGetElapsedTimef() - started > interval)// || p.z < 0)
    {
        alive = false;
    }
}

void WaterParticle::draw()
{
    float size = 0.3;
    ofPushMatrix();
    ofTranslate(p);
    //float s = sin(powf(ofMap(ofGetElapsedTimef(), started, started + interval, 0, PI, true), 0.6));
    float s = ofMap(fabsf(p.z), 1000, 0, 0, 1, true) * size;
    ofScale(s, s);
    ofSetColor(255);
    
    vbo.bind();
    texture.bind();
    glDrawArrays(GL_TRIANGLES, 0, numVerts);
    texture.unbind();
    vbo.unbind();
    
    ofPopMatrix();
    
}

void WaterParticle::collide()
{
    alive = false;
}