//
//  Fonts.h
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#ifndef __GavisconApp__Fonts__
#define __GavisconApp__Fonts__

#include "ofMain.h"
#include "ofxFontStash.h"

class Fonts
{
public:
    enum
    {
        ALIGN_LEFT,
        ALIGN_CENTER,
        ALIGN_RIGHT
    };
    static void load();
    static void draw(string text, ofVec2f p, float size, int align = ALIGN_CENTER);
    static ofRectangle getBBox(string text, ofVec2f p, float size, int align = ALIGN_CENTER);
    static ofxFontStash font;
    
};

#endif /* defined(__GavisconApp__Fonts__) */
