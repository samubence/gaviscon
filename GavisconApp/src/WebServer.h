#pragma once

#include "ofMain.h"
#include "ofxWebServer.h"
#include "WebServerListener.h"
#include "ofxWebServerBaseRouteHandler.h"
#include "ofxWebServerBaseRoute.h"

class WebServerRouteHandler : public ofxWebServerBaseRouteHandler
{
public:
    struct Settings;

    WebServerRouteHandler(const Settings& _settings = Settings());
    virtual ~WebServerRouteHandler();
    void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response);

    virtual void sendErrorResponse(HTTPServerResponse& response);

    struct Settings {

        string defaultIndex;
        string documentRoot;

        ofxWebServerBaseRouteHandler::Settings route;
        Settings();
    };



protected:
    Settings settings;
};

typedef WebServerRouteHandler::Settings Settings;

//------------------------------------------------------------------------------
class WebServerRoute : public ofxWebServerBaseRoute {
public:

    WebServerRoute(const Settings& _settings) : settings(_settings) {
        ofDirectory documentRootDirectory(settings.documentRoot);
        if(!documentRootDirectory.exists()) {
            documentRootDirectory.create();
        }
    }
    virtual ~WebServerRoute() { }

    HTTPRequestHandler* createRequestHandler(const HTTPServerRequest& request) {
        if(WebServerRouteHandler::matchRoute(URI(request.getURI()), settings.route)) {
            return new WebServerRouteHandler(settings);
        } else {
            return NULL;
        }
    }

    static ofPtr<WebServerRoute> Instance(const Settings& settings = Settings()) {
        return ofPtr<WebServerRoute>(new WebServerRoute(settings));
    }

protected:

    Settings settings;

};

//------------------------------------------------------------------------------
class WebServer : public ofxWebServer {
public:
    WebServer(WebServerListener * listener, unsigned short port = 8080, const string& documentRoot = "DocumentRoot") {
        WebServer::listener = listener;
        settings.port = port;
        WebServerRouteHandler::Settings defaultRouteSettings;
        defaultRouteSettings.documentRoot = documentRoot;
        addRoute(WebServerRoute::Instance(defaultRouteSettings));
    }

    WebServer(unsigned short port = 8080, const string& documentRoot = "DocumentRoot") {
        settings.port = port;
        WebServerRouteHandler::Settings defaultRouteSettings;
        defaultRouteSettings.documentRoot = documentRoot;
        addRoute(WebServerRoute::Instance(defaultRouteSettings));
    }
    static WebServerListener * listener;
    virtual ~WebServer() { }
};
