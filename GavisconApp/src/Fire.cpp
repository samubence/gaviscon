//
//  Fire.cpp
//  GavisconApp
//
//  Created by bence samu on 06/08/14.
//
//

#include "Fire.h"
/*
vector<ofTexture*> Fire::seq;

void Fire::init()
{
    for (int i = 50; i < 500; i+= 10)
    {
        ofTexture * t = new ofTexture();
        ofLoadImage(*t, "gfx/game/fireseq/f" + ofToString(i, 4, '0') + ".png");
        //t->setAnchorPercent(0.5, 1);
        seq.push_back(t);
    }
}
*/

Fire::Fire(ofVec3f _p)
{
    p = _p;
    alive = true;
    hitCount = 0;
    hitsMin = 100;
    hitsMax = 200;
    
    hitAreaMin = 50;
    hitAreaMax = 300;
    
    started = ofGetElapsedTimef();
    interval = 10;
    
    //cFrame = 0;
}

void Fire::update(float dt)
{
    float pt = ofMap(ofGetElapsedTimef(), started, started + interval, 0, 1, true);
    
    hitArea = ofMap(pt, 0, 1, hitAreaMin, hitAreaMax);
    
    int n = ofMap(pt, 0, 1, 1, 4);
    float fireSizeX = ofMap(pt, 0, 1, 5, hitArea);
    float fireSizeY = ofMap(pt, 0, 1, 5, hitAreaMin);
    hitsToDie = ofMap(pt, 0, 1, hitsMin, hitsMax);
    
    
    for (int i = 0; i < n; i++)
    {
        if (ofRandom(0, 100) < 40)
        {
            ofVec3f rnd = ofVec2f(ofRandomf() * fireSizeX, ofRandomf() * fireSizeY);
            ofVec3f fp = p + rnd;
            ofNotifyEvent(fireEvents, fp);
        }
    }
    
    /*
    cFrame += 0.5;
    if (cFrame >= seq.size()) cFrame = seq.size() / 2;
    */
    if (hitCount > hitsToDie)
    {
        alive = false;
    }
}

void Fire::draw()
{
    
    ofSetColor(0, 20);
    ofNoFill();
    ofCircle(p, hitArea);
    
    ofNoFill();
    ofSetColor(0);
    
    float barWidth = 100;
    float barHeight = 4;
    ofRect(p.x - barWidth / 2., p.y - 50, barWidth, barHeight);
    
    float ptc = ofMap(hitCount, 0, hitsToDie, 0, 1);
    ofFill();
    ofSetColor(255);
    ofRect(p.x - barWidth / 2., p.y - 50, barWidth * ptc, barHeight);
    /*
    ofEnableBlendMode(OF_BLENDMODE_ADD);
    ofPushMatrix();
    ofTranslate(p);
    ofTexture * t = seq[cFrame];
    t->draw(-t->getWidth() / 2., 0);
    ofPopMatrix();
    ofEnableBlendMode(OF_BLENDMODE_ALPHA);
     */
}