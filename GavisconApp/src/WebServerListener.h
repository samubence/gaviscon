#ifndef __WebServerListener_h__
#define __WebServerListener_h__

#include "ofMain.h"

class WebServerListener
{
    public:
        virtual void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response){};
};

#endif

