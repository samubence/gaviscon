//
//  Parameters.cpp
//  GavisconApp
//
//  Created by bence samu on 17/08/14.
//
//

#include "Parameters.h"
#include "ofxXmlSettings.h"

// DIFFICULTY

float Parameters::levelPct = 0;

float Parameters::foodIntervalMinEasy = 1;
float Parameters::foodIntervalMaxEasy = 3;

float Parameters::foodIntervalMinHard = 0.5;
float Parameters::foodIntervalMaxHard = 2;

float Parameters::foodGravMin = 0.06;
float Parameters::foodGravMax = 0.1;

int Parameters::fireCountMin = 5;
int Parameters::fireCountMax = 8;

float Parameters::fadeIntervalEasy = 6;
float Parameters::fadeIntervalHard = 4;

float Parameters::wetFilledPercent = 0.5;

float Parameters::paintSizeMin = 40;
float Parameters::paintSizeMax = 30;

int Parameters::numPlayers = 1;

// TIMING

float Parameters::gameInterval = 1.5 * 60;
float Parameters::introInterval = 3;
float Parameters::nextLevelInterval = 6;
float Parameters::claimDelayInterval = 3;
float Parameters::welcomeInterval = 3;


// SCORES
float Parameters::scoreBounce = 15;
float Parameters::scoreLevel = 200;
float Parameters::scoreLevelMultiply = 1.3;
float Parameters::scoreTimeMultiply = 3;

// TWO PLAYERS

float Parameters::paintSizeScale = 0.7;
float Parameters::fadeIntervalScale = 0.7;

// Gun

float Parameters::motionScaleX = 2;
float Parameters::motionScaleY = 2;

void Parameters::load()
{
    ofxXmlSettings xml;
    if (xml.load("parameters.xml"))
    {
        xml.pushTag("parameters");
        foodIntervalMinEasy = xml.getValue("foodIntervalMinEasy", foodIntervalMinEasy);
        foodIntervalMinHard = xml.getValue("foodIntervalMinHard", foodIntervalMinHard);
        foodIntervalMinHard = xml.getValue("foodIntervalMinHard", foodIntervalMinHard);
        foodIntervalMaxHard = xml.getValue("foodIntervalMaxHard", foodIntervalMaxHard);
        foodGravMin = xml.getValue("foodGravMin", foodGravMin);
        foodGravMax = xml.getValue("foodGravMax", foodGravMax);
        fireCountMin = xml.getValue("fireCountMin", fireCountMin);
        fireCountMax = xml.getValue("fireCountMax", fireCountMax);
        fadeIntervalEasy = xml.getValue("fadeIntervalEasy", fadeIntervalEasy);
        fadeIntervalHard = xml.getValue("fadeIntervalHard", fadeIntervalHard);
        wetFilledPercent = xml.getValue("wetFilledPercent", wetFilledPercent);
        paintSizeMin = xml.getValue("paintSizeMin", paintSizeMin);
        paintSizeMax = xml.getValue("paintSizeMax", paintSizeMax);
        welcomeInterval = xml.getValue("welcomeInterval", welcomeInterval);
        gameInterval = xml.getValue("gameInterval", gameInterval);
        introInterval = xml.getValue("introInterval", introInterval);
        nextLevelInterval = xml.getValue("nextLevelInterval", nextLevelInterval);
        claimDelayInterval = xml.getValue("claimDelayInterval", claimDelayInterval);
        paintSizeScale = xml.getValue("paintSizeScale", paintSizeScale);
        fadeIntervalScale = xml.getValue("fadeIntervalScale", fadeIntervalScale);
        motionScaleX = xml.getValue("motionScaleX", motionScaleX);
        motionScaleY = xml.getValue("motionScaleY", motionScaleY);
        xml.popTag();
    }
}
