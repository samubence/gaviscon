//
//  FireParticle.cpp
//  FastFireTest
//
//  Created by bence samu on 06/08/14.
//
//

#include "FireParticle.h"

ofVbo FireParticle::vbo;
int FireParticle::numVerts;
vector<ofTexture*> FireParticle::flameTexs;


void FireParticle::init()
{
    float w, h;
    for (int i = 0; i < 16; i++)
    {
        ofTexture * t = new ofTexture();
        ofLoadImage(*t, "gfx/game/fire/f" + ofToString(i) + ".jpg");
        flameTexs.push_back(t);
        w = t->getWidth();
        h = t->getHeight();
    }

    ofMesh msh;
    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));

    msh.addTexCoord(ofVec2f(w, 0));
    msh.addVertex(ofVec2f(w/2., -h/2.));

    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));

    msh.addTexCoord(ofVec2f(w, h));
    msh.addVertex(ofVec2f(w/2., h/2.));

    msh.addTexCoord(ofVec2f(0, h));
    msh.addVertex(ofVec2f(-w/2., h/2));

    msh.addTexCoord(ofVec2f(0, 0));
    msh.addVertex(ofVec2f(-w/2., -h/2.));

    vbo.setMesh(msh, GL_STATIC_DRAW);
    numVerts = msh.getNumVertices();
}

FireParticle::FireParticle(ofVec3f _p)
{
    p = _p;
    alive = true;
    interval = ofRandom(1, 2) * 1.4;

    started = ofGetElapsedTimef();
    rotation = ofRandomf() * 360;
    rotSpeed = ofRandomf() * 2;

    seed = ofVec2f(ofRandomf() * 100, ofRandomf() * 100);
    amp = ofVec2f(ofRandom(0.005, 0.05), ofRandom(0.06, 0.1));
}

void FireParticle::update(float dt)
{
    if (ofGetElapsedTimef() - started > interval)
    {
        alive = false;
    }
    else
    {
        float tFreq = 20;

        float vx = ofMap(ofNoise(ofGetElapsedTimef() * tFreq, 123.456, seed.x), 0, 1, -amp.x, amp.x);
        float vy = ofMap(ofNoise(ofGetElapsedTimef() * tFreq, 987.654, seed.y), 0, 1, 0, amp.y);
        v += ofVec2f(vx, vy);
        //v += ofVec2f(0, 0.1);
        p += v;

        rotation += rotSpeed;
    }

    int n = ofMap(ofGetElapsedTimef(), started, started + interval, 0, flameTexs.size() - 1, true);
    n = flameTexs.size() / 2;
    texture = flameTexs[n];
}

void FireParticle::draw()
{
    ofEnableBlendMode(OF_BLENDMODE_ADD);
    float ptp = powf(ofMap(ofGetElapsedTimef(), started, started + interval, 0, 1, true), 5);
    float pt = ofMap(ofGetElapsedTimef(), started, started + interval, 0, 1, true);
    
    float scale = 1;//ofMap(pt, 0, 1, 0.1, 2);
    float th = 0.2;
    
    ofSetColor(ofMap(pt, 0, 1, 100, 0));
    
    if (pt < th)
    {
        scale = ofMap(pt, 0, th, 0, 1);
        ofSetColor(ofMap(pt, 0, th, 0, 100));
    }
    ofPushMatrix();
    ofTranslate(p);
    ofScale(scale, scale);
    ofRotate(rotation, 0, 0, 1);
    
    //ofSetColor(255);

    vbo.bind();
    texture->bind();
    glDrawArrays(GL_TRIANGLES, 0, numVerts);
    texture->unbind();
    vbo.unbind();

    ofPopMatrix();
    ofEnableAlphaBlending();
}
