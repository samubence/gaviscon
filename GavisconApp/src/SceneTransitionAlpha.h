#ifndef __SceneTransitionAlpha_h__
#define __SceneTransitionAlpha_h__

#include "ofMain.h"
#include "BaseSceneTransition.h"

class SceneTransitionAlpha : public BaseSceneTransition
{
    public:
        void draw(ofFbo * fbo1, ofFbo * fbo2, float p)
        {
            if (p < 1)
            {
                ofSetColor(255);
                fbo1->draw(0, 0);
            }
            ofSetColor(255, 255 * p);
            fbo2->draw(0, 0);
        }
};

#endif

