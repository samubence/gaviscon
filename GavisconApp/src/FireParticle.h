//
//  FireParticle.h
//  GameplayDemo
//
//  Created by bence samu on 20/07/14.
//
//

#ifndef GameplayDemo_FireParticle_h
#define GameplayDemo_FireParticle_h

#include "ofMain.h"
#include "RenderListElement.h"

class FireParticle : public RenderListElement
{
public:
    FireParticle(ofVec3f _p);
    static void init();
    void update(float dt);
    void draw();
    
    bool alive;
    ofVec3f v;
    float friction;
    ofTexture * texture;
    float started, interval;
    float rotation, rotSpeed;
    ofVec2f seed;
    ofVec2f amp;
    
    static ofVbo vbo;
    static int numVerts;
    static vector<ofTexture*> flameTexs;
};

#endif
