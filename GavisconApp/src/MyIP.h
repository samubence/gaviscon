#ifndef __MyIP_h__
#define __MyIP_h__

#ifdef TARGET_WIN32

#include "ofMain.h"

#include <winsock2.h>

struct IPv4
{
    unsigned char b1, b2, b3, b4;
};

class MyIP
{
    public:

        static string getLocal()
        {
            IPv4 _myIp;
            getMyIP(_myIp);
            return ofToString((int)_myIp.b1) + "." + ofToString((int)_myIp.b2) + "." + ofToString((int)_myIp.b3) + "." + ofToString((int)_myIp.b4);
        }

        static string getGlobal()
        {
            ofHttpResponse myIp = ofLoadURL("http://www.binaura.net/bnc/temp/getmyip.php");
            return myIp.data.getText();
        }

        static bool getMyIP(IPv4 & myIP)
        {
            char szBuffer[1024];

            WSADATA wsaData;
            WORD wVersionRequested = MAKEWORD(2, 0);
            if(::WSAStartup(wVersionRequested, &wsaData) != 0)
                return false;

            if(gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR)
            {
              #ifdef WIN32
              WSACleanup();
              #endif
              return false;
            }

            struct hostent *host = gethostbyname(szBuffer);
            if(host == NULL)
            {
              #ifdef WIN32
              WSACleanup();
              #endif
              return false;
            }

            //Obtain the computer's IP
            myIP.b1 = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b1;
            myIP.b2 = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b2;
            myIP.b3 = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b3;
            myIP.b4 = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b4;

            #ifdef WIN32
            WSACleanup();
            #endif
            return true;
        }
};

#endif
#endif

