#ifndef __BaseSceneTransition_h__
#define __BaseSceneTransition_h__

#include "ofMain.h"

class BaseSceneTransition
{
    public:
        virtual void init(int width, int height){};
        virtual void draw(ofFbo * fbo1, ofFbo * fbo2, float transitionPercent){};
};

#endif

