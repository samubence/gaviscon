#ifndef __BaseScene_h__
#define __BaseScene_h__

#include "ofMain.h"

class BaseScene
{
    public:
        BaseScene(){sceneAlive = false;};
        ~BaseScene(){};

        virtual void setup(){};
        virtual void destroy(){};
        virtual void update(){};
        virtual void draw(){};
    
    virtual void onTouch(ofVec2f p){};
    virtual void onDrag(ofVec2f p){};
    virtual void onRelease(ofVec2f p){};
    virtual void onPan(ofVec2f p){};
    virtual void onZoom(ofVec2f p, float amount){};

        int sceneId;
        ofEvent<int> sceneEvent;
        float sceneWidth, sceneHeight;
        bool sceneAlive;
};

#endif

