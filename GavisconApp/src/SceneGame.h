//
//  SceneGame.h
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#ifndef __GavisconApp__SceneGame__
#define __GavisconApp__SceneGame__

#include "ofMain.h"
#include "BaseScene.h"
#include "Layer.h"
#include "LayerLevel.h"
#include "RenderList.h"
#include "Fire.h"
#include "FireParticle.h"
#include "WaterParticle.h"
#include "Gun.h"
#include "Object.h"
#include "FoodFactory.h"
#include "LevelDisplay.h"

#define NUM_USER 2

class SceneGame : public BaseScene
{
public:

    enum
    {
        EVENT_TAKE_PHOTO,
        EVENT_WIN,
        EVENT_LOOSE
    };

    void init();
    void setup();
    void destroy();

    void update();
    void draw();

    void start();
    void fireEvent(ofVec3f & e);
    void setPlayers(vector<string> _userNames);
    string getPlayers();
    void resetGame(int level);
    void takePhoto();
    void addScore(float amt);

    void levelFadedEvent(bool & e);

    ofCamera cam;
    Layer lBg, lFg, lIntro, lIntroBg;
    LayerLevel lLevel;
    RenderList renderList;
    vector<Layer*> layers;
    ofVec3f camPos;

    int level, numLevels;

    float surfacePct;

    LevelDisplay levelDisplay;

    Gun * gun[NUM_USER];
    vector<Gun*> guns;
    string userNames[NUM_USER];

    FoodFactory foodFactory;
    vector<FireParticle*> firePts;
    vector<Fire*> fire;
    vector<WaterParticle*> waterPts;

    float pFrame;

    float endStarted;
    bool playing;
    bool isPlayingTimeup;

    float gameStarted;
    bool win;
    bool gameIsOver;

    float score, tScore;

    float timeLeft2timeUp;

    float fadeStarted;

    float lastPhoto, photoInterval;
    int photoCount, numPhotos;

    float zoomPct, zoomStarted;

    ofVec3f typoStomachPos, typoAcidPos;
    float typoAlpha;
};

#endif /* defined(__GavisconApp__SceneGame__) */
