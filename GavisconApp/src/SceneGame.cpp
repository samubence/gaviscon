//
//  SceneGame.cpp
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#include "SceneGame.h"
#include "ofxXmlSettings.h"
#include "Texts.h"
#include "Parameters.h"
#include "Sfx.h"
#include "Acid.h"

void SceneGame::init()
{
    foodFactory.setup();

    FireParticle::init();
    WaterParticle::init();
    Acid::init();

    ofAddListener(levelDisplay.fadedEvent, this, &SceneGame::levelFadedEvent);

    ofxXmlSettings xml;
    xml.load("settings.xml");


    lBg.load(&xml, "layerBg");
    lLevel.load(&xml, "layerLevel");
    lFg.load(&xml, "layerFg");
    lIntro.load(&xml, "layerIntro");
    lIntroBg.load(&xml, "layerIntroBg");

    lLevel.setup();

    layers.push_back(&lIntroBg);
    layers.push_back(&lIntro);

    layers.push_back(&lBg);
    layers.push_back(&lLevel);
    layers.push_back(&lFg);

    camPos.set(0, 0, 700);
    cam.setPosition(camPos);
    cam.setFov(70);
    cam.setNearClip(10);
    cam.setFarClip(10000);

    endStarted = -1;

    gun[0] = new Gun();
    gun[0]->setup();
    gun[1] = new Gun();
    gun[1]->setup();

    vector<string> usrs; usrs.push_back("player1");//usrs.push_back("player2");
    setPlayers(usrs);

    numLevels = 3;
    timeLeft2timeUp = 10;

    gameStarted = -1;

    numPhotos = 4;
    photoInterval = (Parameters::gameInterval) / (float)numPhotos;
}

void SceneGame::setup()
{
    //printf("SceneGame::setup()\n");

    gameStarted = -1;
    zoomStarted = ofGetElapsedTimef();

    tScore = score = 0;
    Parameters::levelPct = 0;

    lLevel.clearMask();

    photoCount = 0;
    lastPhoto = -1;

    Sfx::playStartNew();
    isPlayingTimeup = false;

    fadeStarted = -1;
    gameIsOver = false;
}

void SceneGame::start()
{
    level = 0;
    Sfx::fadeGameLoop(true);
    resetGame(level);

    //levelDisplay.start(Texts::getText(Texts::LEVEL1));

    gameStarted = ofGetElapsedTimef();
    lastPhoto = ofGetElapsedTimef() - photoInterval;
    photoCount = 0;
    win = false;
}

void SceneGame::destroy()
{
    // clear all fire
    for (int i = 0; i < fire.size(); i++)
    {
        fire[i]->alive = false;
    }
    for (int i = 0; i < firePts.size(); i++)
    {
        firePts[i]->alive = false;
    }

    Sfx::fadeTimeUpLoop(false);
}

void SceneGame::update()
{
    float dt = ofGetElapsedTimef() - pFrame;
    pFrame = ofGetElapsedTimef();

    // ZOOM
    ofVec3f camHome(0, 0, 700);
    ofVec3f camTarget(-486.328125, -2609.375000, 10000);
    zoomPct = ofMap(ofGetElapsedTimef(), zoomStarted, zoomStarted + Parameters::introInterval, 0, 1, true);
    //zoomPct = ofMap(ofGetMouseY(), 0, ofGetHeight(), 0, 1, true);
    //zoomPct = 0.5;
    float zoom = ofMap(cos(zoomPct * PI), 1, -1, 1, 0);


    camPos = camHome + (camTarget - camHome) * powf(zoom, 1.4);

    lIntro.layerAlpha = ofMap(zoom, 0.05, 0.001, 1, 0, true);
    lIntroBg.layerAlpha = ofMap(zoom, 0.8, 0.6, 1, 0., true) * lIntro.layerAlpha;
    //lFg.layerAlpha = lIntro.layerAlpha;

    if (zoom > 0.7) typoAlpha = ofMap(zoom, 0.8, 0.7, 0, 1, true);
    else typoAlpha = ofMap(zoom, 0.1, 0.05, 1, 0, true);

    // TIME UP
    float timePtc = ofMap(ofGetElapsedTimef(), gameStarted, gameStarted + Parameters::gameInterval, 0, 1, true);
    float timeLeft = Parameters::gameInterval - (ofGetElapsedTimef() - gameStarted);

    if (timeLeft < timeLeft2timeUp && !isPlayingTimeup && gameStarted != -1 && !gameIsOver)
    {
        isPlayingTimeup = true;
        Sfx::fadeTimeUpLoop(true);
    }

    // CAMERA MOVEMENT + GUN POSITION
    float f = 0.1;
    float nx = (ofNoise(123.456, ofGetElapsedTimef() * f) - 0.5) * 50;
    float ny = (ofNoise(987.654, ofGetElapsedTimef() * f) - 0.5) * 50;
    cam.setGlobalPosition(camPos + ofVec2f(nx, ny));

    ofVec3f cp = cam.getGlobalPosition();
    gun[0]->update(ofVec3f(cp.x - 60, cp.y -80, 600));
    gun[1]->update(ofVec3f(cp.x + 60, cp.y -80, 600));

    // SCORE

    score += (tScore - score) * 0.1;

    // PHOTO
    if (lastPhoto != -1 && ofGetElapsedTimef() - lastPhoto > photoInterval)
    {
        takePhoto();
    }


    if (gameStarted == -1)
    {
        if (zoom < 0.001)
            start();
    }
    else
    {
        if (ofGetMousePressed())
        {
            gun[0]->lookAt(ofVec3f(ofMap(ofGetMouseX(), 0, ofGetWidth(), -1000, 1000) + cp.x, ofMap(ofGetMouseY(), 0, ofGetHeight(), 1000, -1000), 0));
            gun[1]->lookAt(ofVec3f(ofMap(ofGetMouseX(), 0, ofGetWidth(), -1000, 1000) + cp.x, ofMap(ofGetMouseY(), 0, ofGetHeight(), 1000, -1000), 0));
        }

        if (!gameIsOver)
        {
            if (endStarted == -1 && ofGetElapsedTimef() - gameStarted > Parameters::gameInterval && !win)
            {
                levelDisplay.start();//Texts::getText(Texts::LOOSE));
                endStarted = ofGetElapsedTimef();
                playing = false;
            }

            if (lLevel.wetPtc == 1 && fire.size() == 0 && playing)
            {
                lLevel.fade();
                endStarted = ofGetElapsedTimef();
                playing = false;
                if (level + 1 < numLevels)
                {
                    fadeStarted = ofGetElapsedTimef();
                    levelDisplay.start(Texts::getText(Texts::LEVEL1 + level + 1));
                    addScore(Parameters::scoreLevel);
                    takePhoto();
                    Sfx::fadeGameLoop(false);
                    Sfx::playNextLevel();
                }
                else
                {
                    takePhoto();
                    levelDisplay.start();//Texts::getText(Texts::WIN));

                    win = true;
                    addScore(Parameters::scoreLevel);
                    tScore *= ofMap(ofGetElapsedTimef(), gameStarted, gameStarted + Parameters::gameInterval, Parameters::scoreTimeMultiply, 1, true);
                }
            }
        }

    }
    surfacePct += (100 * lLevel.wetPtc - surfacePct) * 0.1;
    if (!playing) surfacePct = 100;


    if (levelDisplay.started == -1)
        lLevel.updateAcid();

    if (playing)
    {
        // ADD FOOD
        int fireCountMax = ofMap(Parameters::levelPct, 0, 1, 6, 8);
        if (fire.size() < fireCountMax)
        {
            foodFactory.update();
        }

        // ADD WATER

        for (int j = 0; j < guns.size(); j++)
        {
            for (int i = 0; i < 3; i++)
            {
                float speed = ofRandom(25, 30);
                float dst = ofRandom(5, 10);
                ofVec3f p(gun[j]->getGlobalPosition() + gun[j]->getDirection() * dst);
                ofVec3f v(gun[j]->getDirection() * speed);
                WaterParticle * w = new WaterParticle(p, v);
                waterPts.push_back(w);
            }
        }
    }

    // update LAYERS

    for (int i = 0; i < layers.size(); i++)
    {
        layers[i]->update(0);
    }

    // update FOOD

    for (vector<Object*>::iterator i = foodFactory.objs.begin(); i != foodFactory.objs.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update(dt);
            if ((*i)->killStarted == -1)
            {
                ofVec2f lp = lLevel.getLocalPoint((*i)->p);
                if (lLevel.hitTest(lp))
                {
                    (*i)->kill();

                    if (lLevel.isPointInsideWet(ofVec2f(lp.x, lLevel.textureWet.getHeight() - lp.y)) == false)
                    {
                        if (playing)
                        {
                            (*i)->drawn();
                            Fire * f = new Fire((*i)->p + ofVec3f(0, 0, 10));
                            ofAddListener(f->fireEvents, this, &SceneGame::fireEvent);
                            fire.push_back(f);
                            Sfx::playBad();
                        }
                        else
                        {
                            (*i)->bounce();
                            addScore(Parameters::scoreBounce);
                            Sfx::playGood();
                        }
                    }
                    else
                    {
                        (*i)->bounce();
                        addScore(Parameters::scoreBounce);
                        Sfx::playGood();
                    }
                }
            }
            i++;
        }
        else
        {
            delete *i;
            i = foodFactory.objs.erase(i);
        }
    }

    // WATER



    for (vector<WaterParticle*>::iterator i = waterPts.begin(); i != waterPts.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update(dt);

            if (fabsf((*i)->p.z - lLevel.p.z) < 30)
            {
                // collision test
                ofVec2f lp = lLevel.getLocalPoint((*i)->p);

                if (lLevel.isPointInsideSurface(lp))
                {
                    //Score::add((*i)->p, Score::SCORE_SURFACE);
                }

                if (lLevel.hitTest(lp))
                {
                    lLevel.hit(lp);
                    (*i)->alive = false;

                }
            }

            if (fabsf((*i)->p.z - lFg.p.z) < 30)
            {

                // collision test
                /*
                 ofVec2f lp = lFg.getLocalPoint((*i)->p);
                 if (lFg.hitTest(lp))
                 {
                 (*i)->alive = false;
                 }
                 */
            }

            i++;
        }
        else
        {
            delete *i;
            i = waterPts.erase(i);
        }
    }

    // FIRE
    for (vector<Fire*>::iterator i = fire.begin(); i != fire.end(); )
    {
        if ((*i)->alive)
        {
            (*i)->update(dt);

            for (vector<WaterParticle*>::iterator j = waterPts.begin(); j != waterPts.end(); j)
            {
                float dsqr = (*j)->p.distance((*i)->p);
                if (dsqr < (*i)->hitArea)
                {
                    (*i)->hitCount++;
                    j = waterPts.erase(j);
                }
                else j++;
            }

            i++;
        }
        else
        {
            ofRemoveListener((*i)->fireEvents, this, &SceneGame::fireEvent);
            delete *i;
            i = fire.erase(i);
        }
    }

    // FIRE PARTICLESfire

    for (vector<FireParticle*>::iterator i = firePts.begin(); i != firePts.end();)
    {
        if ((*i)->alive)
        {
            (*i)->update(dt);
            i++;
        }
        else
        {
            delete *i;
            i = firePts.erase(i);
        }
    }

    // RENDER LIST
    renderList.clear();

    for (int i = 0; i < layers.size(); i++)
    {
        if (layers[i]->layerAlpha > 0)
            renderList.add(layers[i]);
    }
    if (gameStarted != -1)  // if we have zoomed in, only display guns and water
    {
        for (int i = 0; i < waterPts.size(); i++)
        {
            renderList.add(waterPts[i]);
        }
        /*
        for (int i =0 ; i < guns.size(); i++)
        {
            renderList.add(guns[i]);
        }*/
        renderList.add(gun[0]);
        renderList.add(gun[1]);
    }
    for (int i = 0; i < foodFactory.objs.size(); i++)
    {
        renderList.add(foodFactory.objs[i]);
    }

    for (int i = 0; i < firePts.size(); i++)
    {
        renderList.add(firePts[i]);
    }

    levelDisplay.update();
}

void SceneGame::draw()
{
    ofClear(0);
    cam.begin();
    ofEnableDepthTest();
    renderList.draw();
    ofDisableDepthTest();
    /*
    for (int i = 0; i < fire.size(); i++)
    {
        fire[i]->draw();
    }
     */
    cam.end();

    if (gameStarted != -1)
    {
        ofVec2f shadowShift(3, 3);

        // DRAW SURFACE PERCENT
        string surfacePctStr = ofToString((int)surfacePct) + " %";
        float surfacePctSize = 150;
        ofVec2f surfacePctPos(sceneWidth / 2., sceneHeight - 20);

        ofSetColor(50, 90);
        Fonts::draw(surfacePctStr, surfacePctPos, surfacePctSize);

        // DRAW TIMER
        float alpha = 1;

        float timePtc = ofMap(ofGetElapsedTimef(), gameStarted, gameStarted + Parameters::gameInterval, 0, 1, true);
        float timeLeft = Parameters::gameInterval - (ofGetElapsedTimef() - gameStarted);
        if (timeLeft < timeLeft2timeUp)
        {
            float freq = 20;
            alpha = (sin(ofGetElapsedTimef() * freq) * 0.5 + 0.5);
        }

        int millis = (int)((1 - timePtc) * Parameters::gameInterval * 1000);
        int min = millis / 1000 / 60;
        int sec = (millis - (min * 1000 * 60)) / 1000;

        string timeStr = ofToString(min, 2, '0') + ":" + ofToString(sec, 2, '0');// + ":" + ofToString(mil, 2, '0');
        ofVec2f timePos(sceneWidth - 270, 100);

        float timeSize = 150;

        if (fadeStarted == -1)
        {
            ofSetColor(0, 50 * alpha);
            Fonts::draw(timeStr, timePos + shadowShift, timeSize, Fonts::ALIGN_LEFT);

            ofSetColor(255, 200 * alpha);
            Fonts::draw(timeStr, timePos, timeSize, Fonts::ALIGN_LEFT);

            ofRectangle timeView(sceneWidth - 270, 120, 260, 3);

            ofSetColor(255, 50);
            ofFill();
            ofRect(timeView);

            ofSetColor(255, 200 * alpha);
            ofFill();
            ofRect(timeView.x, timeView.y, timeView.width * (1- timePtc), timeView.height);

        }
        // DRAW SCORES
        ofVec2f scorePos(27.000000, 46.000000);//ofGetMouseX(), ofGetMouseY());
        //printf("%f, %f\n", scorePos.x, scorePos.y);
        string scoreStr = ofToString((int)score);
        string scoreTitle = Texts::getText(Texts::SCORE) + ": " + scoreStr;
        float scoreSize = 80;
        ofSetColor(0, 50);
        Fonts::draw(scoreTitle, scorePos + shadowShift, scoreSize * 0.8, Fonts::ALIGN_LEFT);
        //Fonts::draw(scoreStr, scorePos + ofVec2f(0, scoreSize * 0.8) + shadowShift, scoreSize, Fonts::ALIGN_LEFT);
        ofSetColor(255, 200);
        Fonts::draw(scoreTitle, scorePos + shadowShift, scoreSize * 0.8, Fonts::ALIGN_LEFT);
        //Fonts::draw(scoreStr, scorePos + ofVec2f(0, scoreSize * 0.8) + shadowShift, scoreSize, Fonts::ALIGN_LEFT);


        // DRAW LEVEL

        float levelSize = 60;
        ofVec2f levelPos(sceneWidth / 2., 50);
        string levelStr = Texts::getText(Texts::LEVEL1 + level);
        ofSetColor(255);
        Fonts::draw(levelStr, levelPos, levelSize, Fonts::ALIGN_CENTER);

        // DRAW LEVEL DISPLAY and FADER
        levelDisplay.draw(ofRectangle(0, 0, sceneWidth, sceneHeight));//ofVec2f(sceneWidth / 2., sceneHeight / 2.));

    }
    else    // INTRO
    {
        // DRAW TYPO

        typoStomachPos.set(-44.921875, 111.979126, lIntro.p.z + 10);
        typoAcidPos.set(-132.812500, -484.375000, lIntro.p.z + 10);//ofMap(ofGetMouseX(), 0, ofGetWidth(), -1000, 1000), ofMap(ofGetMouseY(), 0, ofGetHeight(), -1000, 1000), lIntro.p.z);

        ofVec2f t1Start = cam.worldToScreen(typoStomachPos);
        ofVec2f t2Start = cam.worldToScreen(typoAcidPos);

        //printf("%f %f\n", typoAcidPos.x, typoAcidPos.y);

        ofVec2f t1End = t1Start + ofVec2f(-90, -20);
        ofVec2f t2End = t2Start + ofVec2f(60, 30);

        ofSetColor(230, typoAlpha * 150);
        ofFill();
        ofCircle(t1Start, 2);
        ofCircle(t1End, 2);
        ofCircle(t2Start, 2);
        ofCircle(t2End, 2);
        ofSetLineWidth(2);
        ofLine(t1Start, t1End);
        ofLine(t2Start, t2End);
        ofLine(t1End, t1End + ofVec2f(-Fonts::getBBox(Texts::getText(Texts::STOMACH), t1End, 80).width, 0));
        ofLine(t2End, t2End + ofVec2f(Fonts::getBBox(Texts::getText(Texts::ACID), t1End, 80).width, 0));

        ofSetColor(255, typoAlpha * 250);
        Fonts::draw(Texts::getText(Texts::STOMACH), t1End + ofVec2f(0, -5), 80, Fonts::ALIGN_RIGHT);
        Fonts::draw(Texts::getText(Texts::ACID), t2End + ofVec2f(0, -5), 80, Fonts::ALIGN_LEFT);

    }
}

void SceneGame::levelFadedEvent(bool &e)
{
    if (ofGetElapsedTimef() - gameStarted > Parameters::gameInterval && !win)
    {
        gameIsOver = true;
        for (int i =0 ; i < numPhotos; i++) takePhoto();
        int e = EVENT_LOOSE;
        ofNotifyEvent(sceneEvent, e);
    }
    else if (level + 1 < numLevels)
    {
        gameStarted += (ofGetElapsedTimef() - fadeStarted);
        fadeStarted = -1;

        resetGame(level + 1);
        Sfx::fadeGameLoop(true);
    }
    else
    {
        level++;
        gameIsOver = true;
        for (int i = 0 ; i < numPhotos; i++) takePhoto();
        int e = EVENT_WIN;
        ofNotifyEvent(sceneEvent, e);
    }
}

void SceneGame::fireEvent(ofVec3f &e)
{
    e.z += ofRandomf() * 10;
    firePts.push_back(new FireParticle(e));

    while (firePts.size() > 5000)
    {
        delete firePts[0];
        firePts.erase(firePts.begin());
    }
}

void SceneGame::resetGame(int _level)
{
    level = _level;
    Parameters::levelPct = ofMap(_level, 0, numLevels - 1, 0, 1, true);
    endStarted = -1;
    lLevel.reset();
    foodFactory.reset();
    playing = true;
}

void SceneGame::setPlayers(vector<string> _userNames)
{
    if (_userNames.size() > 2) return;
    guns.clear();

    for (int i = 0; i < NUM_USER; i++) userNames[i] = "";
    for (int i = 0; i < _userNames.size(); i++)
    {
        userNames[i] = _userNames[i];
        guns.push_back(gun[i]);
    }
    Parameters::numPlayers = _userNames.size();
}

string SceneGame::getPlayers()
{
    if (userNames[0] != "" && userNames[1] == "") return userNames[0];
    if (userNames[0] == "" && userNames[1] != "") return userNames[1];
    if (userNames[0] != "" && userNames[1] != "") return userNames[0] + " & " + userNames[1];
    return "";
}

void SceneGame::takePhoto()
{
    if (photoCount < numPhotos)
    {
        //printf("TAKE PHOTO\n");
        lastPhoto = ofGetElapsedTimef();
        int e = EVENT_TAKE_PHOTO;
        ofNotifyEvent(sceneEvent, e);
        photoCount++;
    }
}

void SceneGame::addScore(float amt)
{
    tScore += amt * (level > 0 ? level * Parameters::scoreLevelMultiply : 1);
}
