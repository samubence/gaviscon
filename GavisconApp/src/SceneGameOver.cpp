//
//  SceneGameOver.cpp
//  GavisconApp
//
//  Created by bence samu on 08/08/14.
//
//

#include "SceneGameOver.h"
#include "ofxXmlSettings.h"
#include "Fonts.h"
#include "Texts.h"
#include "Sfx.h"
#include "Parameters.h"
#include "LetterBoxView.h"

void SceneGameOver::init()
{
    ofLoadImage(bg, "gfx/gameOver/bg.png");
    ofLoadImage(fg, "gfx/gameOver/fg.png");
    ofLoadImage(claimFg, "gfx/gameOver/claim.png");

    photoTex = NULL;
    acid.setup(ofRectangle(0, -100, sceneWidth, sceneHeight + 200), ACID_BUBBLE);
    acid.iteration = 1;
    fadeInterval = 1;
    loadScore();    // to load scores
}

void SceneGameOver::loadScore()
{
    scores.clear();
    
    ofxXmlSettings xml;
    if (xml.load("score.xml"))
    {
        xml.pushTag("scores");
        int n = xml.getNumTags("element");
        for (int i = 0; i < n; i++)
        {
            xml.pushTag("element", i);
            scores.push_back(Score(xml.getValue("score", 0), xml.getValue("name", "")));
            xml.popTag();
        }
        xml.popTag();
    }
    
    std::sort(scores.begin(), scores.end(), compScore);
}

void SceneGameOver::setup()
{
    //printf("SceneGameOver::setup()\n");
    loadScore();

    started = ofGetElapsedTimef();

    if (level == 0 || level == 1)
        Sfx::playGameOverTimeout();
    else
        Sfx::playGameOverWin();

    isPlaying = false;
}

void SceneGameOver::destroy()
{
    Sfx::fadeGameOverLoop(false);
}

void SceneGameOver::setPhoto(ofTexture *texture)
{
    photoTex = texture;
}

void SceneGameOver::addScore(int _level, Score score)
{
    level = (int)ofMap(_level, 1, 3, 0, 2, true);
    int maxLength = 22;
    if (score.name.length() > maxLength)
    {
        score.name = score.name.substr(0, maxLength-3);
        score.name += "...";
    }
    //printf("addScore: %i %s\n", score.value, score.name.c_str());

    scores.push_back(score);
    std::sort(scores.begin(), scores.end(), compScore);
    cScore = 0;

    for (int i = 0; i < scores.size(); i++)
    {
        //printf("%i  : %i %s\n", i, scores[i].value, scores[i].name.c_str());
        if (scores[i].value == score.value && scores[i].name == score.name)
        {
            cScore = i;
            //break;
        }
    }
    //printf("cScore: %i\n", cScore);
    save();
}

void SceneGameOver::save()
{
    ofxXmlSettings xml;

    int tScores = xml.addTag("scores");
    xml.pushTag("scores", tScores);
    for (int i = 0; i < scores.size(); i++)
    {
        int tElement = xml.addTag("element");
        xml.pushTag("element", tElement);
        xml.addValue("score", scores[i].value);
        xml.addValue("name", scores[i].name);
        xml.popTag();
    }
    xml.popTag();
    xml.save("score.xml");
}

void SceneGameOver::clearScore()
{
    scores.clear();
    save();
    cScore = 0;
}

bool SceneGameOver::compScore( Score v1, Score v2)
{
    return v1.value > v2.value;
}

void SceneGameOver::update()
{
    if (ofGetFrameNum() % 5 == 0) acid.add();
    acid.update();
}

void SceneGameOver::draw()
{
    float fade = ofMap(ofGetElapsedTimef(), started + Parameters::claimDelayInterval, started + Parameters::claimDelayInterval + fadeInterval, 0, 1, true);
    if (fade == 1 && !isPlaying)
    {
        isPlaying = true;
        Sfx::fadeGameOverLoop(true);
    }
    ofClear(0);

    ofSetColor(255);
    bg.draw(0, 0);

    acid.draw();

    ofSetColor(255, 255 * fade);
    fg.draw(0, 0);

    drawScore(fade);
    drawPhoto(fade);

    ofSetColor(255, 255 * (1 - fade));
    claimFg.draw(0, 0);
    drawClaim(1 - fade);
}

void SceneGameOver::drawPhoto(float a)
{
    //ofVec2f photoSize = ofVec2f(640, 480) * 0.9;
    ofRectangle photoView(ofGetMouseX(), ofGetMouseY(), 500, 700);
    //printf("%f, %f, %f, %f\n", photoView.x, photoView.y, photoView.width, photoView.height);
    photoView.set(500, 32.000000, 500.000000, 700.000000);
    ofRectangle texView(0, 0, photoTex->getWidth(), photoTex->getHeight());
    LetterBoxView::begin(texView, photoView);
    ofSetColor(255, 255 * a);
    photoTex->draw(0, 0);
    ofNoFill();
    LetterBoxView::end();
}

void SceneGameOver::drawScore(float a)
{
    scoreView.set(38.000000, 82, 423.000000, 660);//, ofGetMouseX(), ofGetMouseY());
    //printf("%f, %f, %f, %f\n", scoreView.x, scoreView.y, scoreView.width, scoreView.height);
    //ofSetColor(0);ofNoFill();ofRect(scoreView);

    float scoreSize = 40;

    ofSetColor(247, 207, 22, 255 * a);
    Fonts::draw(Texts::getText(Texts::HIGH_SCORE), ofVec2f(scoreView.x + scoreView.width / 2., scoreView.y - 20), scoreSize * 1.2, Fonts::ALIGN_CENTER);

    float y = scoreView.y + scoreSize;

    for (int i = 0; i < min((int)scores.size(), 3); i++)
    {
        ofSetColor(0, 255 * a);
        Fonts::draw(ofToString(i + 1) + ". " + scores[i].name, ofVec2f(scoreView.x, y), scoreSize, Fonts::ALIGN_LEFT);
        string score = ofToString(scores[i].value);
        if (score.length() >= 4) score.insert(1, ".");
        Fonts::draw(score, ofVec2f(scoreView.x + scoreView.width, y), scoreSize, Fonts::ALIGN_RIGHT);
        y += scoreSize;
    }

    y = scoreView.y + scoreSize - (cScore - 6) * scoreSize;

    for (int i = 0; i < scores.size(); i++)
    {
        if (y >= scoreView.y + scoreSize * 5 && y < scoreView.y + scoreView.height)
        {
            ofSetColor(i == cScore ? ofMap(sin(ofGetElapsedTimef() * 5), -1, 1, 0, 255) : 50, 255 * a);
            Fonts::draw(ofToString(i + 1) + ". " + scores[i].name, ofVec2f(scoreView.x, y), scoreSize, Fonts::ALIGN_LEFT);
            string score = ofToString(scores[i].value);
            if (score.length() >= 4) score.insert(1, ".");
            Fonts::draw(score, ofVec2f(scoreView.x + scoreView.width, y), scoreSize, Fonts::ALIGN_RIGHT);
        }
        y += scoreSize;
    }
}

void SceneGameOver::drawClaim(float a)
{
    // DRAW MESSAGE
    ofRectangle msgView(ofGetMouseX(), ofGetMouseY(), 500, 110);
    //printf("%f, %f, %f, %f\n", msgView.x, msgView.y, msgView.width, msgView.height);
    msgView.set(265.000000, 234.000000, 500.000000, 110.000000);
    float msgSize = 50;
    ofVec2f msgPos(msgView.x + msgView.width / 2., msgView.y + msgView.height / 2.);
    ofSetColor(0, 255 * a);
    //Fonts::draw(Texts::getText(Texts::SCORE_LOW + level), msgPos, msgSize, Fonts::ALIGN_CENTER);
    int numLines = 10;
    string text = Texts::getText(Texts::SCORE_LOW + level);
    vector<string> lines = ofSplitString(text, "\\");
    for (int i = 0; i < lines.size(); i++)
    {
      Fonts::draw(lines[i], msgPos + ofVec2f(0, i * msgSize * 0.8), msgSize, Fonts::ALIGN_CENTER);
    }

    //ofSetColor(255);ofNoFill();ofRect(msgView);
}
