//
//  Layer.h
//  GavisconApp
//
//  Created by bence samu on 01/08/14.
//
//

#ifndef __GavisconApp__Layer__
#define __GavisconApp__Layer__

#include "ofMain.h"
#include "RenderListElement.h"
#include "ofxXmlSettings.h"

class Layer : public RenderListElement
{
public:
    Layer();
    virtual void load(ofxXmlSettings * xml, string tagName);
    virtual void save(ofxXmlSettings * xml, string tagName);
    virtual void update(float dt);
    bool hitTest(ofVec2f _p);
    ofVec2f getLocalPoint(ofVec3f _p);
    
    void draw();
    virtual void drawContent();
    ofTexture texture;
    unsigned char * pixels;
    float scale;
    string fileName;
    float layerAlpha;
};

#endif /* defined(__GavisconApp__Layer__) */
