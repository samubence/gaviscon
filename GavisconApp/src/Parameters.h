//
//  Parameters.h
//  GavisconApp
//
//  Created by bence samu on 17/08/14.
//
//

#ifndef __GavisconApp__Parameters__
#define __GavisconApp__Parameters__

class Parameters
{
public:
    // LEVELS
    static float levelPct;
    static int numPlayers;
    // fire
    static int fireCountMin, fireCountMax;
    // food
    static float foodIntervalMinEasy, foodIntervalMinHard;
    static float foodIntervalMaxEasy, foodIntervalMaxHard;
    static float foodGravMin, foodGravMax;

    // layer
    static float paintSizeMin, paintSizeMax;
    static float fadeIntervalEasy, fadeIntervalHard;
    static float wetFilledPercent;

    // TIMING
    static float gameInterval;
    static float introInterval;
    static float nextLevelInterval;
    static float claimDelayInterval;
    static float welcomeInterval;

    // SCORE
    static float scoreBounce, scoreLevel;
    static float scoreLevelMultiply, scoreTimeMultiply;

    // two players

    static float paintSizeScale;
    static float fadeIntervalScale;

    // gun

    static float motionScaleX;
    static float motionScaleY;

    static void load();
};

#endif /* defined(__GavisconApp__Parameters__) */
