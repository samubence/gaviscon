#ifndef __Photo_h__
#define __Photo_h__

#include "ofMain.h"

class Photo
{
    public:
        Photo();
        ~Photo();

        void setup();
        void clear();
        void update();
        void shoot();
        void generateComp();
        void save(string fileName);
    
        vector<ofTexture*> photos;
        vector<ofVec4f> photoPos;

        ofFbo comp;
        ofVideoGrabber vGrabber;

        int photoWidth, photoHeight;
        ofRectangle compView;

        bool shootImage;

        int cPhoto;
        ofTexture fg;
};

#endif

