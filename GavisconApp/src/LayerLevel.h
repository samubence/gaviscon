//
//  LayerLevel.h
//  GavisconApp
//
//  Created by bence samu on 01/08/14.
//
//

#ifndef __GavisconApp__LayerLevel__
#define __GavisconApp__LayerLevel__

#include "ofMain.h"
#include "Layer.h"
#include "Acid.h"

class LayerLevel : public Layer
{
public:
    void setup();
    void update(float dt);
    void drawContent();
    void hit(ofVec2f _p);
    void load(ofxXmlSettings * xml, string tagName);
    void save(ofxXmlSettings * xml, string tagName);
    
    void clearMask();
    void setupAcid();

    bool isPointInsideWet(ofVec2f _p);
    bool isPointInsideSurface(ofVec2f _p);
    
    void fade();
    void reset();
    
    void updateAcid();

    string fileNameWet;
    ofTexture textureWet;
    ofShader shader;
    ofFbo maskFbo;
    ofFbo acidFbo;
    Acid acid, acidUp;

    vector<ofVec3f> hits;
    vector<ofVec2f> wetPts;
    vector<ofPoint> surface;
    float wetRadius, wetRadiusSqr;
    float wetPtc;

    ofMesh surfaceMesh;

    float fadeStarted, fadeInterval;
};

#endif /* defined(__GavisconApp__LayerLevel__) */
