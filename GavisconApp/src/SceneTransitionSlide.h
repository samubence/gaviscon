#ifndef __SceneTransitionSlide_h__
#define __SceneTransitionSlide_h__

#include "ofMain.h"
#include "BaseSceneTransition.h"

class SceneTransitionSlide : public BaseSceneTransition
{
    public:
        void draw(ofFbo * fbo1, ofFbo * fbo2, float p)
        {
            ofSetColor(255);
            p = powf(p, 3);
            if (p < 1)
            {
                fbo1->draw(0, ofMap(p, 0, 1, 0, fbo1->getHeight()));
            }
            fbo2->draw(0, ofMap(p, 0, 1, -fbo1->getHeight(), 0));
        }
};

#endif

