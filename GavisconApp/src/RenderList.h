#ifndef RENDERLIST_H_INCLUDED
#define RENDERLIST_H_INCLUDED

#include "ofMain.h"
#include "RenderListElement.h"

class RenderList
{
    public:

        RenderList();
        ~RenderList();

        void add(RenderListElement * element);
        void clear();
        void draw();

        vector<RenderListElement*> renderList;
};

static bool compareZ(const RenderListElement * a, const RenderListElement * b)
{
    return (a->p.z < b->p.z);
};


#endif // RENDERLIST_H_INCLUDED
