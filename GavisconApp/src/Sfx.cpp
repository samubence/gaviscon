//
//  Sfx.cpp
//  GavisconApp
//
//  Created by bence samu on 21/08/14.
//
//

#include "Sfx.h"
#include "ofxXmlSettings.h"

ofSoundPlayer Sfx::sBad;
ofSoundPlayer Sfx::sGood;
ofSoundPlayer Sfx::sNextLevel;
ofSoundPlayer Sfx::sStartNew;
ofSoundPlayer Sfx::sGameOverWin;
ofSoundPlayer Sfx::sGameOverTimeout;
ofSoundPlayer Sfx::sWelcomeLoop;
ofSoundPlayer Sfx::sGameLoop;
ofSoundPlayer Sfx::sGameOverLoop;
ofSoundPlayer Sfx::sTimeUpLoop;

float Sfx::welcomeLoopVol;
float Sfx::tWelcomeLoopVol;
float Sfx::gameLoopVol;
float Sfx::tGameLoopVol;
float Sfx::gameOverLoopVol;
float Sfx::tGameOverLoopVol;
float Sfx::timeUpLoopVol;
float Sfx::tTimeUpLoopVol;
float Sfx::gameMaxVol;


void Sfx::init()
{
    ofxXmlSettings xml;
    if (xml.load("sfx.xml"))
    {
        xml.pushTag("sfx");
        sBad.loadSound(xml.getValue("bad", ""));
        sGood.loadSound(xml.getValue("good", ""));
        sNextLevel.loadSound(xml.getValue("nextLevel", ""));
        sStartNew.loadSound(xml.getValue("startGame", ""));
        sTimeUpLoop.loadSound(xml.getValue("timeUpLoop", ""));
        sGameOverWin.loadSound(xml.getValue("gameOverWin", ""));
        sGameOverTimeout.loadSound(xml.getValue("gameOverTimeout", ""));
        sWelcomeLoop.loadSound(xml.getValue("welcomeLoop", ""));
        sGameLoop.loadSound(xml.getValue("gameLoop", ""));
        sGameOverLoop.loadSound(xml.getValue("gameOverLoop", ""));
        gameMaxVol = xml.getValue("gameVol", 1.0);
        xml.popTag();
    }

    sBad.setMultiPlay(true);
    sGood.setMultiPlay(true);

    sWelcomeLoop.setVolume(0);
    sWelcomeLoop.setLoop(true);
    sWelcomeLoop.play();

    sGameLoop.setVolume(0);
    sGameLoop.setLoop(true);
    sGameLoop.play();

    sGameOverLoop.setVolume(0);
    sGameOverLoop.setLoop(true);
    sGameOverLoop.play();

    sTimeUpLoop.setVolume(0);
    sTimeUpLoop.setLoop(true);
    sTimeUpLoop.play();

    welcomeLoopVol = tWelcomeLoopVol = gameLoopVol = gameOverLoopVol = tGameLoopVol = tGameOverLoopVol = timeUpLoopVol = tTimeUpLoopVol = 0;
}

void Sfx::playGood()
{
    sGood.play();
    //printf("playGood\n");
}

void Sfx::playBad()
{
    sBad.play();
    //printf("playBad\n");
}

void Sfx::playNextLevel()
{
    sNextLevel.play();
    //printf("playNextLevel\n");
}

void Sfx::playStartNew()
{
    sStartNew.play();
    //printf("playStartNew\n");
}

void Sfx::playGameOverWin()
{
    sGameOverWin.play();
    //printf("playGameOverWin\n");
}

void Sfx::playGameOverTimeout()
{
    sGameOverTimeout.play();
    //printf("playGameOverTimeout\n");
}

void Sfx::fadeWelcomeLoop(bool fadeIn)
{
    tWelcomeLoopVol = fadeIn;
    //printf("fadeWelcomeLoop: %i\n", fadeIn);
}

void Sfx::fadeGameLoop(bool fadeIn)
{
    tGameLoopVol = fadeIn ? gameMaxVol : 0;
    //printf("fadeGameLoop: %i\n", fadeIn);
}

void Sfx::fadeGameOverLoop(bool fadeIn)
{
    tGameOverLoopVol = fadeIn;
    //printf("fadeGameOverLoop: %i\n", fadeIn);
}

void Sfx::fadeTimeUpLoop(bool fadeIn)
{

    tTimeUpLoopVol = fadeIn;
    //printf("fadeTimeUpLoop: %i\n", fadeIn);
}

void Sfx::update()
{
    float fadeSpeed = 0.05;
    welcomeLoopVol += fadeSpeed * (tWelcomeLoopVol > welcomeLoopVol ? 1 : -1);
    gameLoopVol += fadeSpeed * (tGameLoopVol > gameLoopVol ? 1 : -1);
    gameOverLoopVol += fadeSpeed * (tGameOverLoopVol > gameOverLoopVol ? 1 : -1);
    timeUpLoopVol += fadeSpeed * (tTimeUpLoopVol > timeUpLoopVol ? 1 : -1);

    welcomeLoopVol = ofClamp(welcomeLoopVol, 0, 1);
    gameLoopVol = ofClamp(gameLoopVol, 0, 1);
    gameOverLoopVol = ofClamp(gameOverLoopVol, 0, 1);
    timeUpLoopVol = ofClamp(timeUpLoopVol, 0, 1);

    sGameLoop.setVolume(gameLoopVol);
    sGameOverLoop.setVolume(gameOverLoopVol);
    sTimeUpLoop.setVolume(timeUpLoopVol);
    sWelcomeLoop.setVolume(welcomeLoopVol);

    //printf("%f %f\n", gameLoopVol, timeUpLoopVol);
}
