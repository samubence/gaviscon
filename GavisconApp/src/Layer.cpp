//
//  Layer.cpp
//  GavisconApp
//
//  Created by bence samu on 01/08/14.
//
//

#include "Layer.h"

Layer::Layer()
{
    layerAlpha = 1;
    scale = 1;
    fileName = "";
    p.set(0, 0, 0);
}

void Layer::update(float dt)
{
    
}

void Layer::draw()
{
    ofPushMatrix();
    ofTranslate(p);
    ofScale(scale, scale);
    drawContent();
    ofPopMatrix();
}

void Layer::drawContent()
{
    ofSetColor(255, 255 * layerAlpha);
    texture.draw(0, 0);
}

ofVec2f Layer::getLocalPoint(ofVec3f _p)
{
    _p -= p;
    _p /= scale;
    if (_p.x >= 0 && _p.x < texture.getWidth() && _p.y >= 0 && _p.y < texture.getHeight())
    {
        int x = (int)_p.x;
        int y = (int)_p.y;
        return ofVec2f(x, y);
    }
    return ofVec2f();
}

bool Layer::hitTest(ofVec2f lp)
{
    if (lp != ofVec2f())
    {
        int x = (int)ofClamp(lp.x, 0, texture.getWidth() - 1);
        int y = (int)ofClamp(lp.y, 0, texture.getHeight() - 1);
        y = texture.getHeight() - y;
        int w = texture.getWidth();
        //printf("load: %s\n", fileName.c_str());
        if (pixels[x + y * w] == 255)
        {
            return true;
        }
    }
    return false;
}

void Layer::save(ofxXmlSettings * xml, string tagName)
{
    int layerTag = xml->addTag(tagName);
    xml->pushTag(tagName, layerTag);
    int pTag = xml->addTag("p");
    xml->pushTag("p", pTag);
    xml->addValue("x", p.x);
    xml->addValue("y", p.y);
    xml->addValue("z", p.z);
    xml->popTag();
    xml->addValue("alpha", layerAlpha);
    xml->addValue("fileName", fileName);
    xml->addValue("scale", scale);
    xml->popTag();
}

void Layer::load(ofxXmlSettings * xml, string tagName)
{
    xml->pushTag(tagName);
    p.set(xml->getValue("p:x", 0.f), xml->getValue("p:y", 0.f), xml->getValue("p:z", 0.f));
    layerAlpha = xml->getValue("alpha", 1.0);
    fileName = xml->getValue("fileName", "");
    scale = xml->getValue("scale", 1.0);
    xml->popTag();

    if (fileName != "")
    {
        ofLoadImage(texture, fileName);
    }
    
    ofPixels tmpPixels;
    ofLoadImage(tmpPixels, fileName);
    texture.loadData(tmpPixels);
   // printf("%f %f %s\n", tagName.c_str(), texture.getWidth(), texture.getHeight());
    
    int width = texture.getWidth();
    int height = texture.getHeight();
    
    pixels = new unsigned char[width * height];
    for (int i = 0; i < width * height; i++)
    {
        pixels[i] = tmpPixels.getPixels()[i * tmpPixels.getNumChannels()
                                          + tmpPixels.getNumChannels() - 1];
    }

}