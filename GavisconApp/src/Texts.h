//
//  Texts.h
//  GavisconApp
//
//  Created by bence samu on 12/08/14.
//
//

#ifndef __GavisconApp__Texts__
#define __GavisconApp__Texts__

#include "ofMain.h"

#define NUM_LANGUAGES 2

class Texts
{
public:
    enum LANG
    {
        HUN,
        ENG
    };
    enum ELEMENTS
    {
        STOMACH,
        ACID,
        SCORE,
        LEVEL1,
        LEVEL2,
        LEVEL3,
        SCORE_LOW,
        SCORE_MID,
        SCORE_HIGH,
        HIGH_SCORE
    };

    static void load();
    static int lang;
    static string getText(int n);
    static vector< vector<string> > texts;
};

#endif /* defined(__GavisconApp__Texts__) */
