//
//  LayerLevel.cpp
//  GavisconApp
//
//  Created by bence samu on 01/08/14.
//
//

#include "LayerLevel.h"
#include "Parameters.h"

#define STRINGIFY(A) #A

void LayerLevel::setup()
{
    reset();

    fadeStarted = -1;
    wetPtc = 0;
    
    ofFbo::Settings s;
    s.width = texture.getWidth();
    s.height = texture.getHeight();
    s.internalformat = GL_RGBA;
    //s.numSamples = 2;
    acidFbo.allocate(s);
    
    acidUp.setup(ofRectangle(0, -1500, acidFbo.getWidth(), 1500), ACID_UP);
}

void LayerLevel::reset()
{
    fadeInterval = ofMap(Parameters::levelPct, 0, 1, Parameters::fadeIntervalEasy, Parameters::fadeIntervalHard);
    wetRadius = ofMap(Parameters::levelPct, 0, 1, Parameters::paintSizeMin, Parameters::paintSizeMax);
    
    if (Parameters::numPlayers == 2)
    {
        fadeInterval *= Parameters::fadeIntervalScale;
        wetRadius *= Parameters::paintSizeScale;
    }
    
    wetRadiusSqr = wetRadius * wetRadius;
    
    setupAcid();
    
    fadeStarted = -1;
    clearMask();
}

void LayerLevel::setupAcid()
{
    acid.clear();
    acid.setup(ofRectangle(0, 0, acidFbo.getWidth(), acidFbo.getHeight()), ACID_DOWN);
}

void LayerLevel::clearMask()
{
    maskFbo.begin();
    ofClear(0);
    maskFbo.end();
    wetPtc = 0;
    hits.clear();
}

void LayerLevel::updateAcid()
{
    acid.add();
    
    if (ofRandom(0, 100) < 50)
    {
        int surfN = (int)ofRandom(0, surface.size() - 1);
        if (!isPointInsideWet(surface[surfN])) acidUp.add(surface[surfN]);
    }
    
}

void LayerLevel::update(float dt)
{
    acidUp.update();
    acid.update();
    
    // COUNT WET PTC
    int n = 0;
    for (int i = 0; i < surface.size(); i++)
    {
        if (isPointInsideWet(surface[i])) n++;
    }
    wetPtc = n / (float)surface.size();
    wetPtc = ofMap(wetPtc, 0, 0.85, 0, 1, true);
    
    
    // UPDATE MASK
    maskFbo.begin();
    //ofClear(0, 0, 0, 0);

    // DRAW WET SHAPES
    ofFill();

    for (vector<ofVec3f>::iterator i = hits.begin(); i != hits.end();)
    {
        float p = ofMap(ofGetElapsedTimef(), (*i).z, (*i).z + fadeInterval, 0, 1, true);
        float alpha = ofMap(p, 0, 1, 128, 0);
        ofSetColor(255, alpha);
        ofCircle((*i).x, (*i).y, wetRadius);
        
        if (fadeStarted != -1)
        {
            float sc = 0.1;
            float g = ofNoise((*i).x * sc, (*i).y * sc) * 5;
            (*i).y += g;
        }
        
        if (ofGetElapsedTimef() - (*i).z > fadeInterval || (*i).y > 500)
        {
            i = hits.erase(i);
        }
        else
        {
            i++;
        }
    }
    
    if (fadeStarted == -1)
    {
        // FADE WET
        ofEnableBlendMode(OF_BLENDMODE_SUBTRACT);
        
        for (int i = 0; i < 300; i++)
        {
            ofSetColor(ofRandom(1, 2));
            ofFill();
            ofCircle(ofRandom(0, maskFbo.getWidth()), ofRandom(0, maskFbo.getHeight()), ofRandom(10, 50));
        }
    }

    ofEnableAlphaBlending();

    maskFbo.end();
    
    acidFbo.begin();
    //ofClear(255, 160, 0, 200);
    //ofClear(255, 220, 20, 200);

    ofClear(0, 0, 0, 0);
    ofSetColor(255);
    texture.draw(0, 0);
    ofPushMatrix();
    ofTranslate(0, 10);  // shift bubles, bacause of the bumpy surface
    acid.draw();
    ofPopMatrix();
    
    acidFbo.end();
}

void LayerLevel::fade()
{
    if (fadeStarted == -1)
        fadeStarted = ofGetElapsedTimef();
    
    for (int i = 0; i < surface.size(); i++)
    {
        hit(ofVec2f(surface[i].x, maskFbo.getHeight() - surface[i].y));
    }
}

void LayerLevel::drawContent()
{
    ofPushMatrix();
    ofTranslate(0, texture.getHeight() - 50);

    ofScale(1, -1);
    ofDisableDepthTest();
    acidUp.draw();
    ofEnableDepthTest();
    ofPopMatrix();
    
    ofSetColor(255);
    acidFbo.draw(0, 0);

    ofTranslate(0, 0, 2);
    shader.begin();
    shader.setUniformTexture("maskTex", maskFbo.getTextureReference(), 1);
    textureWet.draw(0, 0);
    shader.end();
}

bool LayerLevel::isPointInsideWet(ofVec2f _p)
{
    for (int i = 0; i < hits.size(); i++)
    {
        if (_p.distanceSquared(hits[i]) < wetRadiusSqr)
        {
            float p = ofMap(ofGetElapsedTimef(), hits[i].z, hits[i].z + fadeInterval, 0, 1, true);
            if (p < Parameters::wetFilledPercent) return true;       // this is how old is the wet surface
        }
    }
    return false;
}

bool LayerLevel::isPointInsideSurface(ofVec2f _p)
{
    _p.y = texture.getHeight() - _p.y;
    for (int i = 0; i < surface.size(); i++)
    {
        if (_p.distanceSquared(surface[i]) < wetRadiusSqr) return true;
    }
    return false;
}

void LayerLevel::hit(ofVec2f _p)
{
    hits.push_back(ofVec3f(_p.x, maskFbo.getHeight() - _p.y, ofGetElapsedTimef()));
}



void LayerLevel::save(ofxXmlSettings * xml, string tagName)
{
    int layerTag = xml->addTag(tagName);
    xml->pushTag(tagName, layerTag);
    int pTag = xml->addTag("p");
    xml->pushTag("p", pTag);
    xml->addValue("x", p.x);
    xml->addValue("y", p.y);
    xml->addValue("z", p.z);
    xml->popTag();
    xml->addValue("fileName", fileName);
    xml->addValue("fileNameWet", fileNameWet);
    xml->addValue("scale", scale);
    xml->popTag();
}

void LayerLevel::load(ofxXmlSettings * xml, string tagName)
{
    xml->pushTag(tagName);
    p.set(xml->getValue("p:x", 0.f), xml->getValue("p:y", 0.f), xml->getValue("p:z", 0.f));
    fileName = xml->getValue("fileName", "");
    fileNameWet = xml->getValue("fileNameWet", "");
    scale = xml->getValue("scale", 1.0);
    xml->popTag();

    ofPixels tmpPixels;
    ofLoadImage(tmpPixels, fileName);
    texture.loadData(tmpPixels);
    ofLoadImage(textureWet, fileNameWet);

    int width = texture.getWidth();
    int height = texture.getHeight();

    pixels = new unsigned char[width * height];
    for (int i = 0; i < width * height; i++)
    {
        pixels[i] = tmpPixels.getPixels()[i * 4 + 3];
    }

    maskFbo.allocate(width,height);

    string shaderProgramFrag = STRINGIFY(
                                         uniform sampler2DRect tex0;
                                         uniform sampler2DRect maskTex;
                                         
                                         void main (void){
                                             vec2 pos = gl_TexCoord[0].st;
                                             vec3 src = texture2DRect(tex0, pos).rgb;
                                             float mask = texture2DRect(maskTex, pos).r * texture2DRect(tex0, pos).a;
                                             gl_FragColor = vec4( src , mask);
                                         });

    shader.setupShaderFromSource(GL_FRAGMENT_SHADER, shaderProgramFrag);
    shader.linkProgram();

    maskFbo.begin();
    ofClear(0,0,0,0);
    maskFbo.end();

    int step = 5;
    for (int x = 0; x < width; x += step)
    {
        for (int y = 0; y < height; y++)
        {
            if (pixels[x + y * width] != 0)
            {
                surface.push_back(ofPoint(x, y));
                break;
            }
        }
    }
    
    surfaceMesh.clear();
    surfaceMesh.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    for (int i = 0; i < surface.size() - 1; i++)
    {
        surfaceMesh.addVertex(surface[i]);
        surfaceMesh.addVertex(surface[i] + ofVec2f(0, 10));
    }
    
}
